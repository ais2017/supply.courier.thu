/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

/**
 *
 * @author admin
 */

public enum OrderState {
    FREE("FREE"),
    DELIVERING("DELIVERING"),
    COMPLETED("COMPLETED"),
    DECLINED("DECLINED");

    protected String value;

    OrderState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }



}
