/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

/**
 *
 * @author admin
 */
public class MapPoint {


    private Float lattitude;

    private Float longtitude;

    private String locationName;

    public MapPoint(Float lattitude, Float longtitude, String locationName) {
        if (lattitude > 360 || lattitude < 0
                || longtitude > 360 || longtitude < 0
                || locationName.length() > 200)
            throw new IllegalArgumentException("Illegal params for class MapPoint");
        this.lattitude = lattitude;
        this.longtitude = longtitude;
        this.locationName = locationName;
    }

    public Float getLattitude() {
        return lattitude;
    }

    public Float getLongtitude() {
        return longtitude;
    }

    public String getLocationName() {
        return locationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MapPoint)) return false;

        MapPoint mapPoint = (MapPoint) o;

        if (getLattitude() != null ? !getLattitude().equals(mapPoint.getLattitude()) : mapPoint.getLattitude() != null)
            return false;
        if (getLongtitude() != null ? !getLongtitude().equals(mapPoint.getLongtitude()) : mapPoint.getLongtitude() != null)
            return false;
        return getLocationName() != null ? getLocationName().equals(mapPoint.getLocationName()) : mapPoint.getLocationName() == null;
    }

    @Override
    public int hashCode() {
        int result = getLattitude() != null ? getLattitude().hashCode() : 0;
        result = 31 * result + (getLongtitude() != null ? getLongtitude().hashCode() : 0);
        result = 31 * result + (getLocationName() != null ? getLocationName().hashCode() : 0);
        return result;
    }
}

