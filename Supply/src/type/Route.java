/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

import java.util.List;

/**
 *
 * @author admin
 */

public class Route {
    private List<MapPoint> route;

    private Integer currentIndex = 0;

    public Route(List<MapPoint> route, Integer currentIndex) {
        if (route == null || currentIndex == null)
            throw new IllegalArgumentException("Invalid params for Route");
        this.route = route;
        this.currentIndex = currentIndex;
    }

    public void moveToNextPoint() {
        if (currentIndex < route.size() - 1)
            ++currentIndex;
        else
            currentIndex = null;
    }

    public boolean isRouteCompleted() {
        return currentIndex == null;
    }

    public void addPoint(MapPoint mapPoint) {
        route.add(mapPoint);
    }

    public MapPoint getPointByIndex(int index) {
        return route.get(index);
    }

    public List<MapPoint> getRoute() {
        return route;
    }

    public MapPoint getCurrentPoint() {
        if (currentIndex >= route.size())
            return null;
        return route.get(currentIndex);
    }

    public MapPoint getNextPoint() {
        if (currentIndex + 1 >= route.size())
            return null;
        return route.get(currentIndex + 1);
    }
}

