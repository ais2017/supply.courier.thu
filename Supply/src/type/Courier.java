/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

import java.util.Collection;

/**
 *
 * @author admin
 */
public class Courier {

    private Long id;
    private String name;
    private String phone;
    private Collection orders;
    private Route route;

   /* public Courier(Long id, String name, String phone, Collection orders, Route route) {
        if (id == null
                || name == null
                || phone == null
                || name.length() > 200
                || phone.length() > 200
                || orders == null || route == null)
            throw new IllegalArgumentException("Invalid params for Courier class");

        this.id = id;
        this.name = name;
        this.phone = phone;
        this.orders = orders;
        this.route = route;
    }*/
    
    public Courier(Long id, String name, String phone, Collection orders) {
        if (id == null
                || name == null
                || phone == null
                || name.length() > 200
                || phone.length() > 200
                || orders == null)
            throw new IllegalArgumentException("Invalid params for Courier class");

        this.id = id;
        this.name = name;
        this.phone = phone;
        this.orders = orders;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public Collection getOrders() {
        return orders;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    void addOrderForReserving(Order order) {
        if (order == null)
            throw new IllegalArgumentException("Invalid params for Courier class");
        orders.add(order);
    }

    void removeOrderWhileReserving(Order order) {
        orders.remove(order);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Courier)) return false;

        Courier courier = (Courier) o;

        return getId().equals(courier.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
