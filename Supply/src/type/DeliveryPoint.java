/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

import java.util.Collection;

/**
 *
 * @author admin
 */

public class DeliveryPoint {

    private MapPoint mapPoint;
    private String name;
    private Collection<Order> orders;

    public DeliveryPoint(MapPoint mapPoint, String name, Collection<Order> orders) {
        if (mapPoint == null
                || name == null
                || name.length() > 200
                || orders == null)
            throw new IllegalArgumentException("Invalid params for DeliveryPoint class");
        this.mapPoint = mapPoint;
        this.name = name;
        this.orders = orders;
    }

    public MapPoint getMapPoint() {
        return mapPoint;
    }

    public String getName() {
        return name;
    }

    public Collection<Order> getOrders() {
        return orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeliveryPoint)) return false;

        DeliveryPoint that = (DeliveryPoint) o;

        if (getMapPoint() != null ? !getMapPoint().equals(that.getMapPoint()) : that.getMapPoint() != null)
            return false;
        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        return getOrders() != null ? getOrders().equals(that.getOrders()) : that.getOrders() == null;
    }

    @Override
    public int hashCode() {
        int result = getMapPoint() != null ? getMapPoint().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getOrders() != null ? getOrders().hashCode() : 0);
        return result;
    }
}


