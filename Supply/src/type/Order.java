/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

import org.joda.time.LocalDate;

/**
 *
 * @author admin
 */

public class Order {

    private Long id;

    private String clientName;

    private String clientPhone;

    private Courier courier;

    private OrderState orderState;

    private DeliveryPoint deliveryPoint;

    private LocalDate lastDeliveryDate;

    private MapPoint targetPoint;

    private int deliveryCounter;

    /*public Order(Long id, String clientName, String clientPhone, OrderState orderState, DeliveryPoint deliveryPoint, MapPoint targetPoint, int deliveryCounter) {
        if (id == null
                || clientName == null
                || clientName.length() > 200
                || clientPhone == null
                || clientPhone.length() > 200
                || orderState == null
                || deliveryPoint == null
                || targetPoint == null)
            throw new IllegalArgumentException("Invalid params for Order class");
        this.id = id;
        this.clientName = clientName;
        this.clientPhone = clientPhone;
        this.orderState = orderState;
        this.deliveryPoint = deliveryPoint;
        this.targetPoint = targetPoint;
        this.deliveryCounter = deliveryCounter;
    }*/
    
    public Order(Long id, String clientName, String clientPhone, OrderState orderState, MapPoint targetPoint, int deliveryCounter) {
        if (id == null
                || clientName == null
                || clientName.length() > 200
                || clientPhone == null
                || clientPhone.length() > 200
                || orderState == null
                || targetPoint == null)
            throw new IllegalArgumentException("Invalid params for Order class");
        this.id = id;
        this.clientName = clientName;
        this.clientPhone = clientPhone;
        this.orderState = orderState;
        this.targetPoint = targetPoint;
        this.deliveryCounter = deliveryCounter;
    }

    public void declineOrder() {
        if (this.orderState != OrderState.DELIVERING)
            throw new IllegalStateException("Order has illegal state");

        Courier courier = this.getCourier();
        courier.removeOrderWhileReserving(this);
        this.setCourier(null);

        lastDeliveryDate = new LocalDate();
        this.deliveryCounter++;
        this.orderState = deliveryCounter < 3 ? OrderState.FREE : OrderState.DECLINED;
    }

    public void reserveOrder(Courier courier) {
        if (this.orderState != OrderState.FREE)
            throw new IllegalStateException("Order has illegal state");
        if (lastDeliveryDate != null && lastDeliveryDate.equals(new LocalDate()))
            throw new IllegalStateException("Order cannot be reserved. " +
                    "Order was already tried to be delevered today. Try it tomorrow");
        this.setCourier(courier);
        courier.addOrderForReserving(this);
        this.orderState = OrderState.DELIVERING;
    }

    public void takeOrder() {
        if (this.orderState != OrderState.DELIVERING)
            throw new IllegalStateException("Order has illegal state");
        this.orderState = OrderState.COMPLETED;
    }

    public void freeOrder() {
        if (this.orderState != OrderState.DELIVERING)
            throw new IllegalStateException("Order has illegal state");
        Courier courier = this.getCourier();
        courier.removeOrderWhileReserving(this);
        this.setCourier(null);
        this.orderState = OrderState.FREE;
    }

    public Long getId() {
        return id;
    }

    public void setLastDeliveryDateForTesting(LocalDate lastDeliveryDate) {
        this.lastDeliveryDate = lastDeliveryDate;
    }

    public String getClientName() {
        return clientName;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public Courier getCourier() {
        return courier;
    }

    public void setCourier(Courier courier) {
        this.courier = courier;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public DeliveryPoint getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setDeliveryPoint(DeliveryPoint deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }

    public MapPoint getTargetPoint() {
        return targetPoint;
    }
}

