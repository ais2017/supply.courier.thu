/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoryTestImpl;

import type.MapPoint;
import type.Route;

import java.util.ArrayList;

/**
 *
 * @author admin
 */

public class RouteRepositoryTestImpl {

    public static Route generateRoute() {
        MapPoint mapPointRostov = new MapPoint(47.25F, 39.75F, "RostovOnDon");
        MapPoint mapPointKrasnodar = new MapPoint(45F, 39F, "Krasnodar");
        ArrayList<MapPoint> mapPoints = new ArrayList<>(3);
        mapPoints.add(mapPointRostov);
        mapPoints.add(mapPointKrasnodar);
        return new Route(mapPoints, 0);
    }
}
