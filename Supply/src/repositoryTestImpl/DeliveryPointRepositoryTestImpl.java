/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoryTestImpl;

import type.DeliveryPoint;
import type.MapPoint;
import repositoryImpl.MapPointRepositoryImpl;

import java.util.Arrays;

/**
 *
 * @author admin
 */
public class DeliveryPointRepositoryTestImpl {
    public final static String MAP_POINT_NAME = "Theatre Square point";

    public static DeliveryPoint generateDeliveryPoint() {
        MapPoint mapPoint = MapPointRepositoryImpl.generateMapPointList().get(1);
        DeliveryPoint deliveryPoint = new DeliveryPoint(mapPoint, MAP_POINT_NAME , Arrays.asList());
        return deliveryPoint;
    }

}

