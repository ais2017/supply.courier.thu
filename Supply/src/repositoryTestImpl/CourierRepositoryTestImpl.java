/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoryTestImpl;

import main.repository.Repository;
import type.Courier;
import type.Order;
import type.Route;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import main.repository.CourierRepository;

/**
 *
 * @author admin
 */
public class CourierRepositoryTestImpl implements CourierRepository {

    private static AtomicLong courierId = new AtomicLong(0);

    public static Long incrementAndGetId() {
        return courierId.incrementAndGet();
    }

    public static Map<Long, Courier> couriers = new HashMap<>();

    public static String CLIENT_NAME = "Timofey Tiylenev";

    public static String CLIENT_PHONE = "88005553535";

    public ArrayList<Courier> getAll() {
        ArrayList <Courier> courierArrayList = new ArrayList<>();
        for(Courier courier:couriers.values()){
            courierArrayList.add(courier);
        }
        return courierArrayList;
    }
    
    public Courier getById(Long id) {
        return couriers.get(id);
    }

    public static Courier generateCourier() {

        //Route route = RouteRepositoryTestImpl.generateRoute();
        long newKey = incrementAndGetId();
        Courier newVal = new Courier(newKey, CLIENT_NAME, CLIENT_PHONE, new ArrayList());
        couriers.put(newKey,newVal);
        return newVal;
    }
}

