/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoryTestImpl;

import type.MapPoint;

import java.util.Arrays;
import java.util.List;
/**
 *
 * @author admin
 */

public class MapPointRepositoryTestImpl {
    public static final String FIRST_MAP_POINT_NAME = "RostovOnDon";
    public static final String SECOND_MAP_POINT_NAME = "Krasnodar";


    public static List<MapPoint> generateMapPointList() {
        MapPoint mapPointRostov = new MapPoint(47.25F, 39.75F, FIRST_MAP_POINT_NAME);
        MapPoint mapPointKrasnodar = new MapPoint(45F, 39F, SECOND_MAP_POINT_NAME);
        return Arrays.asList(mapPointRostov, mapPointKrasnodar);
    }
}
