/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.repository;

import type.Courier;
import type.Order;
import type.OrderState;

import java.util.ArrayList;
import java.util.Collection;
/**
 *
 * @author admin
 */

public interface OrderRepository extends Repository<Order> {
    public Order save(Order order);

    public ArrayList<Order> getAll(OrderState orderState);

    public ArrayList<Order> getAll(OrderState orderState1, OrderState orderState2);

    public ArrayList<Order> getAllByCourier(Courier courier);

    public ArrayList<Order> getAllByCourier(Courier courier, OrderState orderState1, OrderState orderState2);

    public Order getById(Long id);
}
