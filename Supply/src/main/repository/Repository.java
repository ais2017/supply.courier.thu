/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.repository;
import java.util.Collection;

/**
 *
 * @author admin
 */

@org.springframework.stereotype.Repository
public interface Repository<T> {

    public Collection<T> getAll ();

}
