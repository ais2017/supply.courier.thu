/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.repository;

import type.Courier;

/**
 *
 * @author admin
 */
public interface CourierRepository extends Repository<Courier> {
   
    public Courier getById(Long id);
    
}
