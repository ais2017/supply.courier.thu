/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import main.repository.OrderRepository;
import repositoryImpl.OrderRepositoryImpl;
import type.Courier;
import type.Order;
import type.OrderState;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author admin
 */

@Service
public class OrderService {

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    private OrderRepository orderRepository;

    public Collection<Order> getAllOrders() {
        return orderRepository.getAll();
    }

    public ArrayList<Order> getFreeOrders() {
        return orderRepository.getAll(OrderState.FREE);
    }

    public ArrayList<Order> getDeliveringOrders() {
        return orderRepository.getAll(OrderState.DELIVERING);
    }

    public ArrayList<Order> getDeliveringOrFreeOrders() {
        return orderRepository.getAll(OrderState.DELIVERING, OrderState.FREE);
    }

    public ArrayList<Order> getOrdersByCourier(Courier courier) {
        return orderRepository.getAllByCourier(courier);
    }

    public ArrayList<Order> getDeliveringOrCompletedOrdersByCourier(Courier courier) {
        return orderRepository.getAll(OrderState.DELIVERING, OrderState.COMPLETED);
    }

    public Order reserveOrder(Long id, Courier courier) {
        Order order = orderRepository.getById(id);
        if (order == null)
            throw new IllegalArgumentException("Order cannot be null");
        order.reserveOrder(courier);
        return orderRepository.save(order);
    }

    public Order declineOrder(Long id) {
        Order order = orderRepository.getById(id);
        if (order == null)
            throw new IllegalArgumentException("Order cannot be null");
        order.declineOrder();
        return orderRepository.save(order);
    }

    public Order completeOrder(Long id) {
        Order order = orderRepository.getById(id);
        if (order == null)
            throw new IllegalArgumentException("Order cannot be null");
        order.takeOrder();
        return orderRepository.save(order);
    }

    public Order freeOrder(Long id) {
        Order order = orderRepository.getById(id);
        if (order == null)
            throw new IllegalArgumentException("Order cannot be null");
        order.freeOrder();
        return orderRepository.save(order);
    }
}
