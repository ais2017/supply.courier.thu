/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import main.repository.OrderRepository;
import repositoryTestImpl.OrderRepositoryTestImpl;
import type.Order;
import type.OrderState;
import type.Route;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author admin
 */
public class RouteService {

    public RouteService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    private OrderRepository orderRepository;

    public Route buildRoute(type.Courier courier) {
        Collection<Order> orders = orderRepository.getAllByCourier(courier, OrderState.DELIVERING, OrderState.DELIVERING);
        if (orders.size() == 0)
            throw new IllegalStateException("No delivering orders for courier found");
        Route route = new Route(new ArrayList<>(), 0);
        orders.forEach(m -> {
            route.addPoint(m.getDeliveryPoint().getMapPoint());
            route.addPoint(m.getTargetPoint());
        });
        return route;

    }
}