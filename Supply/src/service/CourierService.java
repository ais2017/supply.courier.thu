/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import main.repository.Repository;
import repositoryTestImpl.CourierRepositoryTestImpl;
import type.Courier;

import java.util.ArrayList;
import main.repository.CourierRepository;

/**
 *
 * @author admin
 */
public class CourierService {

    public CourierService(CourierRepository courierRepository){
        this.courierRepository = courierRepository;
    }

    private CourierRepository courierRepository;

    public ArrayList<Courier> getAll() {
        return (ArrayList<Courier>) courierRepository.getAll();
    }
    
    public Courier getById(Long Id) {
        return courierRepository.getById(Id);
    }
}
