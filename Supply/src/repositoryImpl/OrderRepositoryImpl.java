/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoryImpl;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;
import com.mongodb.BasicDBObject;
import com.mongodb.Mongo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import main.repository.OrderRepository;
import type.Courier;
import type.DeliveryPoint;
import type.MapPoint;
import type.Order;
import type.OrderState;


/**
 *
 * @author admin
 */
public class OrderRepositoryImpl implements OrderRepository {
    
    Mongo m = new Mongo();
    DB db = m.getDB("supply");
    DBCollection collOrders = db.getCollection("Orders");
    DBCollection collMapPoints = db.getCollection("MapPoint");
    DBCollection collDeliveryPoint = db.getCollection("DeliveryPoint");
    DBCollection collCourier = db.getCollection("Courier");
    
    
    public Order save(Order order) {
        BasicDBObject newDocument = new BasicDBObject();
        
        newDocument.append("$set", new BasicDBObject().append("clientName", order.getClientName()));
        newDocument.append("$set", new BasicDBObject().append("clientPhone", order.getClientPhone()));
        newDocument.append("$set", new BasicDBObject().append("courierId", 90));
        newDocument.append("$set", new BasicDBObject().append("orderState", order.getOrderState().getValue()));
        BasicDBObject searchQuery = new BasicDBObject().append("_id", order.getId());

        collOrders.update(searchQuery, newDocument);
        return order;
    }

    public Order getById(Long id) {
        /*Получение объекта с помощью объекта-запроса*/
        BasicDBObject query = new BasicDBObject();
        query.put("_id", id);
        DBCursor cur = collOrders.find(query);
        
        if (cur.hasNext()) {
            
            DBObject obj = cur.next();
            /*Получаем отдельные поля объекта*/
            // id
            Object orderId = obj.get("_id");
            Double obd = new Double(String.valueOf(orderId));
            Long Id = obd.longValue();
        
            // clientName && clientPhone
            Object clientN = obj.get("clientName");
            Object clientP = obj.get("clientPhone");
            
            // curierId
            Object courierId = obj.get("courierId");
            Double obdd = new Double(String.valueOf(courierId));
            Long IdCour = obdd.longValue();
        
            // orderState
            Object orderS = obj.get("orderState");
            String StateToString = String.valueOf(orderS);
        
            // deliveryCounter
            Object deliveryCount = obj.get("deliveryCounter");
            double obd1 = new Double(String.valueOf(deliveryCount));
            int deliveryCounter = (int) obd1;
            
            // targetPoint
            Object targetPointId = obj.get("targetPointId"); 
            Double obd2 = new Double(String.valueOf(targetPointId));
            Long Point_Id = obd2.longValue();
            BasicDBObject query_point = new BasicDBObject();
            query_point.put("_id", Point_Id);
            DBCursor pcur = collMapPoints.find(query_point);
            MapPoint targetPoint; 
            
            if (pcur.hasNext()) {
                DBObject obj_point = pcur.next();
                Object lattitude = obj_point.get("lattitude");
                float lat = Float.parseFloat(String.valueOf(lattitude)); 
                Object longitude = obj_point.get("longtitude");
                float flongt = Float.parseFloat(String.valueOf(longitude));   
                Object location = obj_point.get("locationName");       
                targetPoint = new MapPoint(lat, flongt, String.valueOf(location));
            }
            else {
                targetPoint = null;
            }
            
            Order OrderFind = new Order(Id, String.valueOf(clientN), String.valueOf(clientP), OrderState.valueOf(StateToString), targetPoint, deliveryCounter);
            Courier courier = getCourierById(IdCour);
            OrderFind.setCourier(courier);
            return OrderFind;
        }
        else {
            return null;
        }
    }
    
    public Order getByIdForCourier(Long id) {
        /*Получение объекта с помощью объекта-запроса*/
        BasicDBObject query = new BasicDBObject();
        query.put("_id", id);
        DBCursor cur = collOrders.find(query);
        
        if (cur.hasNext()) {
            
            DBObject obj = cur.next();
            /*Получаем отдельные поля объекта*/
            // id
            Object orderId = obj.get("_id");
            Double obd = new Double(String.valueOf(orderId));
            Long Id = obd.longValue();
        
            // clientName && clientPhone
            Object clientN = obj.get("clientName");
            Object clientP = obj.get("clientPhone");
            
            // curierId
            Object courierId = obj.get("courierId");
            Double obdd = new Double(String.valueOf(courierId));
            Long IdCour = obdd.longValue();
        
            // orderState
            Object orderS = obj.get("orderState");
            String StateToString = String.valueOf(orderS);
        
            // deliveryCounter
            Object deliveryCount = obj.get("deliveryCounter");
            double obd1 = new Double(String.valueOf(deliveryCount));
            int deliveryCounter = (int) obd1;
            
            // targetPoint
            Object targetPointId = obj.get("targetPointId"); 
            Double obd2 = new Double(String.valueOf(targetPointId));
            Long Point_Id = obd2.longValue();
            BasicDBObject query_point = new BasicDBObject();
            query_point.put("_id", Point_Id);
            DBCursor pcur = collMapPoints.find(query_point);
            MapPoint targetPoint; 
            
            if (pcur.hasNext()) {
                DBObject obj_point = pcur.next();
                Object lattitude = obj_point.get("lattitude");
                float lat = Float.parseFloat(String.valueOf(lattitude)); 
                Object longitude = obj_point.get("longtitude");
                float flongt = Float.parseFloat(String.valueOf(longitude));   
                Object location = obj_point.get("locationName");       
                targetPoint = new MapPoint(lat, flongt, String.valueOf(location));
            }
            else {
                targetPoint = null;
            }
            
            Order OrderFind = new Order(Id, String.valueOf(clientN), String.valueOf(clientP), OrderState.valueOf(StateToString), targetPoint, deliveryCounter);
            return OrderFind;
        }
        else {
            return null;
        }
    }

    public Collection<Order> getAll() {
        ArrayList<Order> OrdersArrayList = new ArrayList<>();
        List<DBObject> obj = collOrders.find().toArray();
        for (int i = 0; i < obj.size(); i++) {
            
            Object id = obj.get(i).get("_id");
            Double obd3 = new Double(String.valueOf(id));
            Long IdOrd = obd3.longValue();
            Order OrderNew = getById(IdOrd);
            
            OrdersArrayList.add(OrderNew);
        } 
        return OrdersArrayList;
    }

    public ArrayList<Order> getAll(OrderState orderState) {
        ArrayList ordersByOrderState = new ArrayList<>();
        /*Получение объекта с помощью объекта-запроса*/
        BasicDBObject query = new BasicDBObject();
        String state = orderState.getValue();
        query.put("orderState", state);
        DBCursor cur = collOrders.find(query);
        
        while (cur.hasNext()){
            Object id = cur.next().get("_id");
            Double obd = new Double(String.valueOf(id));
            Long IdOrder = obd.longValue();
            Order OrderSt = getById(IdOrder);
            ordersByOrderState.add(OrderSt);
        }
        return ordersByOrderState;
    }

    public ArrayList getAll(OrderState orderState1, OrderState orderState2) {
        ArrayList ordersByOrderState = new ArrayList<>();
        String state1 = orderState1.getValue();
        String state2 = orderState2.getValue();
        /*Получение объекта с помощью объекта-запроса*/
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> orQuery = new ArrayList<BasicDBObject>();
        orQuery.add(new BasicDBObject("orderState", state1));
        orQuery.add(new BasicDBObject("orderState", state2));
        query.put("$or", orQuery); 
        
        DBCursor cur = collOrders.find(query);
        
        while (cur.hasNext()){
            Object id = cur.next().get("_id");
            Double obd = new Double(String.valueOf(id));
            Long IdOrder = obd.longValue();
            Order OrderSt = getById(IdOrder);
            ordersByOrderState.add(OrderSt);
        }
        return ordersByOrderState;
    }

    public ArrayList getAllByCourier(Courier courier) {
        ArrayList ordersByCourier = new ArrayList<>();
        Long Courier_Id = courier.getId();
        /*Получение объекта с помощью объекта-запроса*/
        BasicDBObject query = new BasicDBObject();
        query.put("courierId", Courier_Id);
        DBCursor cur = collOrders.find(query);
        
        while (cur.hasNext()){
            Object id = cur.next().get("_id");
            Double obd = new Double(String.valueOf(id));
            Long IdOrder = obd.longValue();
            Order OrderCour = getById(IdOrder);
            ordersByCourier.add(OrderCour);
        }
        return ordersByCourier;
    }

    public ArrayList<Order> getAllByCourier(Courier courier, OrderState orderState1, OrderState orderState2) {
        ArrayList<Order> ordersByCourier = new ArrayList<>();
        Long Courier_Id = courier.getId();
        String state1 = orderState1.getValue();
        String state2 = orderState2.getValue();
        
        /*Получение объекта с помощью объекта-запроса*/
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> orQuery = new ArrayList<BasicDBObject>();
        List<BasicDBObject> andQuery = new ArrayList<BasicDBObject>();
        orQuery.add(new BasicDBObject("orderState", state1));
        orQuery.add(new BasicDBObject("orderState", state2));
        andQuery.add(new BasicDBObject("$or", orQuery)); 
        andQuery.add(new BasicDBObject("courierId", Courier_Id)); 
        query.put("$and", andQuery);
        
        DBCursor cur = collOrders.find(query);
        
        while (cur.hasNext()){
            Object id = cur.next().get("_id");
            Double obd = new Double(String.valueOf(id));
            Long IdOrder = obd.longValue();
            Order OrderCour = getById(IdOrder);
            ordersByCourier.add(OrderCour);
        }
        return ordersByCourier;
    }

    public Courier getCourierById(Long Id) {
        
        BasicDBObject query = new BasicDBObject();
        query.put("_id", Id);
        DBCursor cur = collCourier.find(query);
        
        if (cur.hasNext()) {
            
            DBObject obj = cur.next();
            
            // id + name + phone
            Object IdCourier = obj.get("_id");
            Object Name = obj.get("name");
            Object Phone = obj.get("phone");
            Double obd = new Double(String.valueOf(IdCourier));
            Long IdCour = obd.longValue();
        
            // orders
            ArrayList ordersByCourier = new ArrayList<>();
            
            BasicDBObject queryOrd = new BasicDBObject();
            queryOrd.put("courierId", IdCour);
            DBCursor ocur = collOrders.find(queryOrd);
        
            while (ocur.hasNext()){
                Object id = ocur.next().get("_id");
                Double obd2 = new Double(String.valueOf(id));
                Long IdOrder = obd2.longValue();
                Order OrderCour = getByIdForCourier(IdOrder);
                ordersByCourier.add(OrderCour);
            }
            Courier findCourier = new Courier(IdCour, String.valueOf(Name), String.valueOf(Phone), ordersByCourier);
            return findCourier;
        }
        else {
            return null;
        }
    }
    
}
