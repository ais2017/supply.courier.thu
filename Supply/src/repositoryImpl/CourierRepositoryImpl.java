/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoryImpl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import java.util.ArrayList;
import java.util.List;
import main.repository.CourierRepository;
import main.repository.Repository;
import service.OrderService;
import type.Courier;
import type.MapPoint;
import type.Order;
import type.OrderState;

/**
 *
 * @author admin
 */
public class CourierRepositoryImpl implements CourierRepository {
    
    Mongo m = new Mongo();
    DB db = m.getDB("supply");
    DBCollection collCourier = db.getCollection("Courier");
    DBCollection collOrders = db.getCollection("Orders");
    DBCollection collMapPoints = db.getCollection("MapPoint");
    
    public ArrayList<Courier> getAll() {
        ArrayList <Courier> courierArrayList = new ArrayList<>();
    
        List<DBObject> obj = collCourier.find().toArray();
        for (int i = 0; i < obj.size(); i++) {
            Object Id = obj.get(i).get("_id");
            Object Name = obj.get(i).get("name");
            Object Phone = obj.get(i).get("phone");
            Double obd = new Double(String.valueOf(Id));
            Long IdCourier = obd.longValue();
            
            ArrayList ordersByCourier = new ArrayList<>();
            /*Получение объекта с помощью объекта-запроса*/
            BasicDBObject query = new BasicDBObject();
            query.put("courierId", IdCourier);
            DBCursor cur = collOrders.find(query);
        
            while (cur.hasNext()){
                Object id = cur.next().get("_id");
                Double obd2 = new Double(String.valueOf(id));
                Long IdOrder = obd2.longValue();
                Order OrderCour = getOrderById(IdOrder);
                ordersByCourier.add(OrderCour);
            }
            Courier newCourier = new Courier(IdCourier, String.valueOf(Name), String.valueOf(Phone), ordersByCourier);
            courierArrayList.add(newCourier);
        }  
        return courierArrayList;
    }
    
    

    public Courier getById(Long Id) {
       
        BasicDBObject query = new BasicDBObject();
        query.put("_id", Id);
        DBCursor cur = collCourier.find(query);
        
        if (cur.hasNext()) {
            
            DBObject obj = cur.next();
           
            // id + name + phone
            Object IdCourier = obj.get("_id");
            Object Name = obj.get("name");
            Object Phone = obj.get("phone");
            Double obd = new Double(String.valueOf(IdCourier));
            Long IdCour = obd.longValue();
        
            // orders
            ArrayList ordersByCourier = new ArrayList<>();
            
            BasicDBObject queryOrd = new BasicDBObject();
            queryOrd.put("courierId", IdCour);
            DBCursor ocur = collOrders.find(queryOrd);
        
            while (ocur.hasNext()){
                Object id = ocur.next().get("_id");
                Double obd2 = new Double(String.valueOf(id));
                Long IdOrder = obd2.longValue();
                Order OrderCour = getOrderById(IdOrder);
                ordersByCourier.add(OrderCour);
            }
            Courier findCourier = new Courier(IdCour, String.valueOf(Name), String.valueOf(Phone), ordersByCourier);
            return findCourier;
        }
        else {
            return null;
        }
    }
    
    public Order getOrderById(Long id) {
        /*Получение объекта с помощью объекта-запроса*/
        BasicDBObject query = new BasicDBObject();
        query.put("_id", id);
        DBCursor cur = collOrders.find(query);
        
        if (cur.hasNext()) {
            
            DBObject obj = cur.next();
            /*Получаем отдельные поля объекта*/
            // id
            Object orderId = obj.get("_id");
            Double obd = new Double(String.valueOf(orderId));
            Long Id = obd.longValue();
        
            // clientName && clientPhone
            Object clientN = obj.get("clientName");
            Object clientP = obj.get("clientPhone");
            
            // curierId
            Object courierId = obj.get("courierId");
            Double obdd = new Double(String.valueOf(courierId));
            Long IdCour = obdd.longValue();
        
            // orderState
            Object orderS = obj.get("orderState");
            String StateToString = String.valueOf(orderS);
        
            // deliveryCounter
            Object deliveryCount = obj.get("deliveryCounter");
            double obd1 = new Double(String.valueOf(deliveryCount));
            int deliveryCounter = (int) obd1;
            
            // targetPoint
            Object targetPointId = obj.get("targetPointId"); 
            Double obd2 = new Double(String.valueOf(targetPointId));
            Long Point_Id = obd2.longValue();
            BasicDBObject query_point = new BasicDBObject();
            query_point.put("_id", Point_Id);
            DBCursor pcur = collMapPoints.find(query_point);
            MapPoint targetPoint; 
            
            if (pcur.hasNext()) {
                DBObject obj_point = pcur.next();
                Object lattitude = obj_point.get("lattitude");
                float lat = Float.parseFloat(String.valueOf(lattitude)); 
                Object longitude = obj_point.get("longtitude");
                float flongt = Float.parseFloat(String.valueOf(longitude));   
                Object location = obj_point.get("locationName");       
                targetPoint = new MapPoint(lat, flongt, String.valueOf(location));
            }
            else {
                targetPoint = null;
            }
            
            Order OrderFind = new Order(Id, String.valueOf(clientN), String.valueOf(clientP), OrderState.valueOf(StateToString), targetPoint, deliveryCounter);
            return OrderFind;
        }
        else {
            return null;
        }
    }
}
