/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoryImpl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import java.util.ArrayList;

import type.MapPoint;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author admin
 */
public class MapPointRepositoryImpl {
  
    public static List<MapPoint> generateMapPointList() {
       
        ArrayList<MapPoint> MapPointArrayList = new ArrayList<>();
        
        Mongo m = new Mongo();
        DB db = m.getDB("supply");
        DBCollection coll = db.getCollection("MapPoint");
        List<DBObject> obj = coll.find().toArray();
        for (int i = 0; i < obj.size(); i++) {
            Object lat = obj.get(i).get("lattitude");
            Object longt = obj.get(i).get("longtitude");
            Object location = obj.get(i).get("locationName");
            String latToString = String.valueOf(lat);
            String longtToString = String.valueOf(longt);
            String nameToString = String.valueOf(location);
            float flat = Float.parseFloat(latToString);
            float flongt = Float.parseFloat(longtToString);
            MapPoint mapPointR = new MapPoint(flat, flongt, nameToString);
            MapPointArrayList.add(mapPointR);
        }  
            return MapPointArrayList;
    }
    
    
}
