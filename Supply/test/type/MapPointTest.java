/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

import org.junit.Assert;
import org.junit.Test;
import repositoryImpl.MapPointRepositoryImpl;

/**
 *
 * @author admin
 */
public class MapPointTest {
    public static final String FIRST_MAP_POINT_NAME = "RostovOnDon";
    public static final String SECOND_MAP_POINT_NAME = "Krasnodar";

    @Test
    public void test() {
        MapPoint mapPointRostov = new MapPoint(47.25F, 39.75F, FIRST_MAP_POINT_NAME);
        Assert.assertTrue(mapPointRostov.getLattitude().equals(47.25F));
        Assert.assertTrue(mapPointRostov.getLongtitude().equals(39.75F));
        Assert.assertEquals(mapPointRostov.getLocationName(), "RostovOnDon");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithException() {
        MapPoint mapPointRostov = new MapPoint(-1F, 39.75F, FIRST_MAP_POINT_NAME);
    }
}

