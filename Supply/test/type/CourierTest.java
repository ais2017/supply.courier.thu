/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

import org.junit.Assert;
import org.junit.Test;
import repositoryTestImpl.CourierRepositoryTestImpl;
import repositoryTestImpl.OrderRepositoryTestImpl;
/**
 *
 * @author admin
 */
public class CourierTest {

    @Test
    public void test() {
        Courier courier = CourierRepositoryTestImpl.generateCourier();
        Order order3 = OrderRepositoryTestImpl.generateOrder();
        Assert.assertEquals(0,courier.getOrders().size() );

        order3.reserveOrder(courier);
        Assert.assertEquals(1,courier.getOrders().size());

        order3.freeOrder();
        Assert.assertEquals(0,courier.getOrders().size());


    }
}
