/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

import org.junit.Assert;
import org.junit.Test;
import repositoryTestImpl.DeliveryPointRepositoryTestImpl;
import repositoryTestImpl.MapPointRepositoryTestImpl;


import static repositoryTestImpl.DeliveryPointRepositoryTestImpl.MAP_POINT_NAME;

/**
 *
 * @author admin
 */
public class DeliveryPointTest {

    @Test
    public void test() {
        DeliveryPoint deliveryPoint = DeliveryPointRepositoryTestImpl.generateDeliveryPoint();
        MapPoint mapPoint = deliveryPoint.getMapPoint();
        Assert.assertEquals(mapPoint.getLocationName(), MapPointRepositoryTestImpl.FIRST_MAP_POINT_NAME);
        Assert.assertEquals(deliveryPoint.getName(), MAP_POINT_NAME);
        Assert.assertEquals(deliveryPoint.getOrders().size(), 0);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithException() {
        MapPoint mapPoint = MapPointRepositoryTestImpl.generateMapPointList().get(0);
        new DeliveryPoint(mapPoint, MAP_POINT_NAME , null);
    }
}

