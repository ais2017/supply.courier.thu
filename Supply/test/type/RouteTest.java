/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package type;

import org.junit.Assert;
import org.junit.Test;

import static repositoryTestImpl.RouteRepositoryTestImpl.generateRoute;

/**
 *
 * @author admin
 */

public class RouteTest {


    @Test
    public void test() {
        Route route = generateRoute();
        MapPoint mapPoint = route.getRoute().get(0);
        MapPoint mapPointNext = route.getRoute().get(1);
        MapPoint mapPointStavropol = new MapPoint(45F, 42F, "Stavropol");
        route.addPoint(mapPointStavropol);

        Assert.assertEquals(route.getCurrentPoint(), mapPoint);
        Assert.assertEquals(route.getNextPoint(), mapPointNext);
        Assert.assertEquals(route.isRouteCompleted(), false);

        route.moveToNextPoint();

        Assert.assertEquals(route.getCurrentPoint(), mapPointNext);
        Assert.assertEquals(route.getNextPoint(), mapPointStavropol);
        Assert.assertEquals(route.isRouteCompleted(), false);

        route.moveToNextPoint();
        route.moveToNextPoint();

        Assert.assertEquals(route.getRoute().size(), 3);
        Assert.assertEquals(route.isRouteCompleted(), true);
    }
}

