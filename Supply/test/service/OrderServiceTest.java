/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import org.junit.Assert;
import org.junit.Test;
import repositoryTestImpl.CourierRepositoryTestImpl;
import repositoryTestImpl.OrderRepositoryTestImpl;
import type.Courier;
import type.Order;
import type.OrderState;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
//Little Mocker Robert Martin

public class OrderServiceTest {

    private OrderService orderService = new OrderService(new OrderRepositoryTestImpl());

    private OrderRepositoryTestImpl orderRepositoryTest = new OrderRepositoryTestImpl();

    private CourierRepositoryTestImpl courierRepositoryTest = new CourierRepositoryTestImpl();

    @Test
    public void reservingOrderTest() {

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getFreeOrders();

        Order chosenOrder = orders.get(0);

        Order order = orderService.reserveOrder(chosenOrder.getId(), courier);
        Assert.assertEquals(chosenOrder.getId(), order.getId());
        Assert.assertEquals(OrderState.DELIVERING, order.getOrderState());
        orderRepositoryTest.clearAllForTesting();

    }


    @Test
    public void completingOrderTest() {

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getDeliveringOrders();

        Order chosenOrder = orders.get(0);

        Order order = orderService.completeOrder(chosenOrder.getId());
        Assert.assertEquals(chosenOrder.getId(), order.getId());
        Assert.assertEquals(OrderState.COMPLETED, order.getOrderState());
        orderRepositoryTest.clearAllForTesting();

    }

    @Test(expected = IllegalStateException.class)
    public void completingOrderTestWithException() {

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getDeliveringOrders();

        Order chosenOrder = orders.get(0);

        orderService.reserveOrder(chosenOrder.getId(), courier);
    }

    public void declineOrderTest() {

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getDeliveringOrders();

        Order chosenOrder = orders.get(0);

        Order order = orderService.declineOrder(chosenOrder.getId());
        Assert.assertEquals(chosenOrder, order);
        Assert.assertEquals(OrderState.DECLINED, order.getOrderState());
        orderRepositoryTest.clearAllForTesting();

    }

    @Test(expected = IllegalStateException.class)
    public void declineOrderTestWithException() {

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getDeliveringOrders();

        Order chosenOrder = orders.get(0);

        Order completedOrder = orderService.completeOrder(chosenOrder.getId());
        orderService.completeOrder(completedOrder.getId());

    }
}
