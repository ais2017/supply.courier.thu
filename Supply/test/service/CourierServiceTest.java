/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import org.junit.Assert;
import org.junit.Test;
import repositoryTestImpl.CourierRepositoryTestImpl;
import repositoryTestImpl.OrderRepositoryTestImpl;
import type.Courier;
import type.Order;
import type.OrderState;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class CourierServiceTest {

    private OrderService orderService = new OrderService(new OrderRepositoryTestImpl());

    private CourierService courierService = new CourierService(new CourierRepositoryTestImpl());

    private OrderRepositoryTestImpl orderRepositoryTest = new OrderRepositoryTestImpl();

    private CourierRepositoryTestImpl courierRepositoryTest = new CourierRepositoryTestImpl();

    @Test
    public void getCourierOrdersTest() {

        Courier courier = courierRepositoryTest.generateCourier();
        Courier courierOther = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);

        ArrayList<Courier> couriers = courierService.getAll();


        Courier chosenCourier = couriers.get(0);
        Courier chosenCourierOther = couriers.get(1);
        Assert.assertEquals(courier, chosenCourier);
        ArrayList<Order> orders = orderService.getOrdersByCourier(chosenCourier);
        Assert.assertEquals(orders.size(), 2);

        ArrayList<Order> ordersOther = orderService.getOrdersByCourier(chosenCourierOther);
        Assert.assertEquals(ordersOther.size(), 0);


    }
}

