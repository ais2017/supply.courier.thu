/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import org.junit.Test;
import repositoryTestImpl.CourierRepositoryTestImpl;
import repositoryTestImpl.OrderRepositoryTestImpl;
import type.Courier;
import type.Order;
import type.OrderState;

/**
 *
 * @author admin
 */

public class RouteServiceTest {

    private OrderService orderService = new OrderService(new OrderRepositoryTestImpl());

    private RouteService routeService = new RouteService(new OrderRepositoryTestImpl());

    private OrderRepositoryTestImpl orderRepositoryTest = new OrderRepositoryTestImpl();

    private CourierRepositoryTestImpl courierRepositoryTest = new CourierRepositoryTestImpl();



    @Test
    public void reservingOrderTest() {

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);

        routeService.buildRoute(courier);
        orderRepositoryTest.clearAllForTesting();
    }

    @Test(expected = IllegalStateException.class)
    public void reservingOrderTestWithException() {

        Courier courier = courierRepositoryTest.generateCourier();

        routeService.buildRoute(courier);
    }
}

