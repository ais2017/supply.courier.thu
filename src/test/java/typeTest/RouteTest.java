package typeTest;

import org.junit.Assert;
import org.junit.Test;
import com.code.type.MapPoint;
import com.code.type.Route;

import static com.code.repositoryTestImpl.RouteRepositoryTestImpl.generateRoute;

public class RouteTest {


    @Test
    public void test() {
        Route route = generateRoute();
        MapPoint mapPoint = route.getRoute().get(0);
        MapPoint mapPointNext = route.getRoute().get(1);
        MapPoint mapPointStavropol = new MapPoint(45F, 42F, "Stavropol");
        route.addPoint(mapPointStavropol);

        Assert.assertEquals(route.getCurrentPoint(), mapPoint);
        Assert.assertEquals(route.getNextPoint(), mapPointNext);
        Assert.assertEquals(route.isRouteCompleted(), false);

        route.moveToNextPoint();

        Assert.assertEquals(route.getCurrentPoint(), mapPointNext);
        Assert.assertEquals(route.getNextPoint(), mapPointStavropol);
        Assert.assertEquals(route.isRouteCompleted(), false);

        route.moveToNextPoint();
        route.moveToNextPoint();

        Assert.assertEquals(route.getRoute().size(), 3);
        Assert.assertEquals(route.isRouteCompleted(), true);
    }
}
