package typeTest;


import org.junit.Assert;
import org.junit.Test;
import com.code.repositoryTestImpl.MapPointRepositoryTestImpl;
import com.code.type.MapPoint;

public class MapPointTest {

    @Test
    public void test() {
        MapPoint mapPointRostov = new MapPoint(47.25F, 39.75F, MapPointRepositoryTestImpl.FIRST_MAP_POINT_NAME);
        Assert.assertTrue(mapPointRostov.getLattitude().equals(47.25F));
        Assert.assertTrue(mapPointRostov.getLongtitude().equals(39.75F));
        Assert.assertEquals(mapPointRostov.getLocationName(), "RostovOnDon");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithException() {
        MapPoint mapPointRostov = new MapPoint(-1F, 39.75F, MapPointRepositoryTestImpl.FIRST_MAP_POINT_NAME);
    }
}
