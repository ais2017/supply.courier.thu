package typeTest;

import org.junit.Assert;
import org.junit.Test;
import com.code.repositoryTestImpl.DeliveryPointRepositoryTestImpl;
import com.code.repositoryTestImpl.MapPointRepositoryTestImpl;
import com.code.type.DeliveryPoint;
import com.code.type.MapPoint;

import static com.code.repositoryTestImpl.DeliveryPointRepositoryTestImpl.MAP_POINT_NAME;


public class DeliveryPointTest {

    @Test
    public void test() {
        DeliveryPoint deliveryPoint = DeliveryPointRepositoryTestImpl.generateDeliveryPoint();
        MapPoint mapPoint = deliveryPoint.getMapPoint();
        Assert.assertEquals(mapPoint.getLocationName(), MapPointRepositoryTestImpl.FIRST_MAP_POINT_NAME);
        Assert.assertEquals(deliveryPoint.getName(), MAP_POINT_NAME);
        Assert.assertEquals(deliveryPoint.getOrders().size(), 0);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithException() {
        MapPoint mapPoint = MapPointRepositoryTestImpl.generateMapPointList().get(0);
        new DeliveryPoint(mapPoint, MAP_POINT_NAME , null);
    }
}
