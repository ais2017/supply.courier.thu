package typeTest;

import com.code.type.Courier;
import com.code.type.DeliveryPoint;
import com.code.type.Order;
import com.code.type.OrderState;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;
import com.code.repositoryTestImpl.CourierRepositoryTestImpl;
import com.code.repositoryTestImpl.DeliveryPointRepositoryTestImpl;
import com.code.repositoryTestImpl.MapPointRepositoryTestImpl;
import com.code.repositoryTestImpl.OrderRepositoryTestImpl;

public class OrderTest {


    @Test
    public void test() {

        Order orderToBeDeclined = OrderRepositoryTestImpl.generateOrder();
        Assert.assertEquals(orderToBeDeclined.getClientName(), OrderRepositoryTestImpl.CLIENT_NAME);
        Assert.assertEquals(orderToBeDeclined.getClientPhone(), OrderRepositoryTestImpl.CLIENT_PHONE);
        Assert.assertEquals(orderToBeDeclined.getDeliveryPoint(), OrderRepositoryTestImpl.DELIVERY_POINT);
        Assert.assertEquals(orderToBeDeclined.getTargetPoint(), OrderRepositoryTestImpl.TARGET_POINT);
        DeliveryPoint deliveryPointOther = DeliveryPointRepositoryTestImpl.generateDeliveryPoint();
        orderToBeDeclined.setDeliveryPoint(deliveryPointOther);
        Assert.assertEquals(orderToBeDeclined.getDeliveryPoint(), deliveryPointOther);

        Assert.assertEquals(orderToBeDeclined.getOrderState(), OrderState.FREE);

        Courier courier = CourierRepositoryTestImpl.generateCourier();
        orderToBeDeclined.reserveOrder(courier);
        Assert.assertEquals(orderToBeDeclined.getOrderState(), OrderState.DELIVERING);

        orderToBeDeclined.declineOrder();
        Assert.assertEquals(orderToBeDeclined.getOrderState(), OrderState.FREE);

        orderToBeDeclined.setLastDeliveryDateForTesting(new LocalDate(2018, 01, 01));
        orderToBeDeclined.reserveOrder(courier);
        orderToBeDeclined.declineOrder();

        orderToBeDeclined.setLastDeliveryDateForTesting(new LocalDate(2018, 01, 01));
        orderToBeDeclined.reserveOrder(courier);
        orderToBeDeclined.declineOrder();
        Assert.assertEquals(orderToBeDeclined.getOrderState(), OrderState.DECLINED);

        //DELIVERED ORDER IS BELOW

        Order orderToBeDelivered = OrderRepositoryTestImpl.generateOrder();

        orderToBeDelivered.reserveOrder(courier);
        Assert.assertEquals(orderToBeDelivered.getOrderState(), OrderState.DELIVERING);

        orderToBeDelivered.freeOrder();
        Assert.assertEquals(orderToBeDelivered.getOrderState(), OrderState.FREE);

        orderToBeDelivered.reserveOrder(courier);
        Assert.assertEquals(orderToBeDelivered.getOrderState(), OrderState.DELIVERING);

        orderToBeDelivered.takeOrder();
        Assert.assertEquals(orderToBeDelivered.getOrderState(), OrderState.COMPLETED);


    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorTest() {

        Order orderForException = new Order(null, null, null, null,
                null, null,0);
    }

    @Test(expected = IllegalStateException.class)
    public void declineOrderTest() {
        Order orderToBeDeclined = OrderRepositoryTestImpl.generateOrder();

        Assert.assertEquals(orderToBeDeclined.getOrderState(), OrderState.FREE);

        orderToBeDeclined.declineOrder();
    }

    @Test(expected = IllegalStateException.class)
    public void declineOrderWithDateTest() {

        Order orderToBeDeclined = OrderRepositoryTestImpl.generateOrder();
        DeliveryPoint deliveryPointOther = DeliveryPointRepositoryTestImpl.generateDeliveryPoint();
        orderToBeDeclined.setDeliveryPoint(deliveryPointOther);
        Courier courier = CourierRepositoryTestImpl.generateCourier();
        orderToBeDeclined.reserveOrder(courier);

        orderToBeDeclined.declineOrder();
        Assert.assertEquals(orderToBeDeclined.getOrderState(), OrderState.FREE);

        orderToBeDeclined.reserveOrder(courier);
        orderToBeDeclined.declineOrder();
    }

    @Test(expected = IllegalStateException.class)
    public void takeOrderTest() {
        Order orderToBeTaken = OrderRepositoryTestImpl.generateOrder();

        Assert.assertEquals(orderToBeTaken.getOrderState(), OrderState.FREE);

        Courier courier = CourierRepositoryTestImpl.generateCourier();
        orderToBeTaken.reserveOrder(courier);
        orderToBeTaken.takeOrder();
        orderToBeTaken.takeOrder();


    }

    @Test(expected = IllegalStateException.class)
    public void reserveOrderTest() {
        Order orderToBeReserved = OrderRepositoryTestImpl.generateOrder();
        Assert.assertEquals(orderToBeReserved.getOrderState(), OrderState.FREE);

        Courier courier = CourierRepositoryTestImpl.generateCourier();
        orderToBeReserved.reserveOrder(courier);
        orderToBeReserved.takeOrder();
        orderToBeReserved.reserveOrder(courier);
    }

    @Test(expected = IllegalStateException.class)
    public void freeOrderTest() {
        Order orderToBeFree = OrderRepositoryTestImpl.generateOrder();
        Assert.assertEquals(orderToBeFree.getOrderState(), OrderState.FREE);

        orderToBeFree.freeOrder();
    }


}
