package typeTest;

import org.junit.Assert;
import org.junit.Test;
import com.code.repositoryTestImpl.CourierRepositoryTestImpl;
import com.code.repositoryTestImpl.OrderRepositoryTestImpl;
import com.code.type.Courier;
import com.code.type.MapPoint;
import com.code.type.Order;
import com.code.type.Route;

public class CourierTest {

    @Test
    public void test() {
        Courier courier = CourierRepositoryTestImpl.generateCourier();
        Order order3 = OrderRepositoryTestImpl.generateOrder();
        Assert.assertEquals(0,courier.getOrders().size() );

        order3.reserveOrder(courier);
        Assert.assertEquals(1,courier.getOrders().size());

        order3.freeOrder();
        Assert.assertEquals(0,courier.getOrders().size());


    }
}
