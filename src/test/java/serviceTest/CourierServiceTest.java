package serviceTest;

import org.junit.Assert;
import org.junit.Test;
import com.code.repositoryTestImpl.CourierRepositoryTestImpl;
import com.code.repositoryTestImpl.OrderRepositoryTestImpl;
import com.code.service.CourierService;
import com.code.service.OrderService;
import com.code.type.Courier;
import com.code.type.Order;
import com.code.type.OrderState;

import java.util.ArrayList;

public class CourierServiceTest {

    private OrderService orderService = new OrderService(new OrderRepositoryTestImpl());

    private CourierService courierService = new CourierService(new CourierRepositoryTestImpl());

    private OrderRepositoryTestImpl orderRepositoryTest = new OrderRepositoryTestImpl();

    private CourierRepositoryTestImpl courierRepositoryTest = new CourierRepositoryTestImpl();

    @Test
    public void orderEntities() {

        Courier courier = courierRepositoryTest.generateCourier();
        Courier courierOther = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);

        ArrayList<Courier> couriers = courierService.getAll();


        Courier chosenCourier = couriers.get(0);
        Courier chosenCourierOther = couriers.get(1);
        Assert.assertEquals(courier, chosenCourier);
        ArrayList<Order> orders = orderService.getOrdersByCourier(chosenCourier);
        Assert.assertEquals(orders.size(), 2);

        ArrayList<Order> ordersOther = orderService.getOrdersByCourier(chosenCourierOther);
        Assert.assertEquals(ordersOther.size(), 0);


    }
}
