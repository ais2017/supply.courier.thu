package serviceTest;

import org.junit.Test;
import com.code.repositoryTestImpl.CourierRepositoryTestImpl;
import com.code.repositoryTestImpl.OrderRepositoryTestImpl;
import com.code.service.OrderService;
import com.code.service.RouteService;
import com.code.type.Courier;
import com.code.type.Order;
import com.code.type.OrderState;

public class RouteServiceTest {

    private OrderService orderService = new OrderService(new OrderRepositoryTestImpl());

    private RouteService routeService = new RouteService(new OrderRepositoryTestImpl());

    private OrderRepositoryTestImpl orderRepositoryTest = new OrderRepositoryTestImpl();

    private CourierRepositoryTestImpl courierRepositoryTest = new CourierRepositoryTestImpl();



    @Test
    public void reservingOrderTest() {

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);

        routeService.buildRoute(courier);
        orderRepositoryTest.clearAllForTesting();
    }

    @Test(expected = IllegalStateException.class)
    public void reservingOrderTestWithException() {

        Courier courier = courierRepositoryTest.generateCourier();

        routeService.buildRoute(courier);
    }
}
