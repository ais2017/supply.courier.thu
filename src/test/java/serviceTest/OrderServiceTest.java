package serviceTest;

import org.junit.Assert;
import org.junit.Test;
import com.code.repositoryTestImpl.CourierRepositoryTestImpl;
import com.code.repositoryTestImpl.OrderRepositoryTestImpl;
import com.code.service.OrderService;
import com.code.type.Courier;
import com.code.type.Order;
import com.code.type.OrderState;

import java.util.ArrayList;
//Little Mocker Robert Martin

public class OrderServiceTest {

    private OrderService orderService = new OrderService(new OrderRepositoryTestImpl());

    private OrderRepositoryTestImpl orderRepositoryTest = new OrderRepositoryTestImpl();

    private CourierRepositoryTestImpl courierRepositoryTest = new CourierRepositoryTestImpl();

    @Test
    public void reservingOrderTest() throws Exception{

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getFreeOrders();

        Order chosenOrder = orders.get(0);

        Order order = orderService.reserveOrder(chosenOrder.getId(), courier);
        Assert.assertEquals(chosenOrder.getId(), order.getId());
        Assert.assertEquals(OrderState.DELIVERING, order.getOrderState());
        orderRepositoryTest.clearAllForTesting();

    }


    @Test
    public void completingOrderTest() throws Exception{

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getDeliveringOrders();

        Order chosenOrder = orders.get(0);

        Order order = orderService.completeOrder(chosenOrder.getId());
        Assert.assertEquals(chosenOrder.getId(), order.getId());
        Assert.assertEquals(OrderState.COMPLETED, order.getOrderState());
        orderRepositoryTest.clearAllForTesting();

    }

    @Test(expected = IllegalStateException.class)
    public void completingOrderTestWithException() throws Exception{

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getDeliveringOrders();

        Order chosenOrder = orders.get(0);

        orderService.reserveOrder(chosenOrder.getId(), courier);
    }

    public void declineOrderTest() throws Exception{

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getDeliveringOrders();

        Order chosenOrder = orders.get(0);

        Order order = orderService.declineOrder(chosenOrder.getId());
        Assert.assertEquals(chosenOrder, order);
        Assert.assertEquals(OrderState.DECLINED, order.getOrderState());
        orderRepositoryTest.clearAllForTesting();

    }

    @Test(expected = IllegalStateException.class)
    public void declineOrderTestWithException() throws Exception{

        Courier courier = courierRepositoryTest.generateCourier();
        orderRepositoryTest.generateOrderCollectionForTesting(courier);
        ArrayList<Order> orders = orderService.getDeliveringOrders();

        Order chosenOrder = orders.get(0);

        Order completedOrder = orderService.completeOrder(chosenOrder.getId());
        orderService.completeOrder(completedOrder.getId());

    }
}
