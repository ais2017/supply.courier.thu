package com.code.repositoryImplClasses;

import com.code.converter.OrderConverter;
import com.code.model.OrderEntity;
import com.code.repository.OrderRepository;
import com.code.repositoryImpl.OrderReposioryImpl;
import com.code.type.Courier;
import com.code.type.Order;
import com.code.type.OrderState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;

@Repository
public class OrderRepositoryImplClass implements OrderRepository {

    @Autowired
    private OrderReposioryImpl orderReposioryImpl;

    @Autowired
    private OrderConverter orderConverter;

    @Override
    public Order save(Order order) throws Exception {
        OrderEntity orderEntity = orderConverter.typeToModel(order);
        OrderEntity orderEntityToReturn = orderReposioryImpl.save(orderEntity);
        Order orderToReturn = orderConverter.modelToType(orderEntityToReturn);
        return orderToReturn;
    }

    @Override
    public ArrayList<Order> getAll(OrderState orderState) {
        Collection<OrderEntity> orderEntities = orderReposioryImpl.findAllByOrderState(orderState);
        return orderConverter.collectionModelToType(orderEntities);
    }

    @Override
    public ArrayList<Order> getAll(OrderState orderState1, OrderState orderState2) {
        return null;
    }

    @Override
    public ArrayList<Order> getAllByCourier(Courier courier) {
        Collection<OrderEntity> orderEntities = orderReposioryImpl.findAllByCourierId(courier.getId());
        return orderConverter.collectionModelToType(orderEntities);
    }

    @Override
    public ArrayList<Order> getAllByCourier(Courier courier, OrderState orderState1, OrderState orderState2) {
        return null;
    }

    @Override
    public Order getById(Long id) {
        OrderEntity orderEntity = orderReposioryImpl.findOne(id);
        return orderConverter.modelToType(orderEntity);
    }

    @Override
    public Collection<Order> getAll() {
        Collection<OrderEntity> orderEntities = orderReposioryImpl.findAll();
        return orderConverter.collectionModelToType(orderEntities);
    }
}
