package com.code.repositoryImplClasses;

import com.code.converter.CourierConverter;
import com.code.converter.OrderConverter;
import com.code.model.CourierEntity;
import com.code.model.OrderEntity;
import com.code.repository.Repository;
import com.code.repositoryImpl.CourierRepositoryImpl;
import com.code.repositoryImpl.OrderReposioryImpl;
import com.code.type.Courier;
import com.code.type.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@org.springframework.stereotype.Repository
public class CourierRepositoryImplClass implements Repository<Courier> {





    @Autowired
    private CourierConverter courierConverter;

    @Qualifier("courierRepositoryImpl")
    @Autowired
    private CourierRepositoryImpl courierRepository;

    @Override
    public Collection<Courier> getAll() {
        ArrayList<CourierEntity> courierEntities = (ArrayList<CourierEntity>)courierRepository.findAll();
        return courierConverter.collectionModelToType(courierEntities);
    }
}
