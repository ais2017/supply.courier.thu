package com.code.service;

import com.code.converter.CourierConverter;
import com.code.model.AccountEntity;
import com.code.model.CourierEntity;
import com.code.repositoryImpl.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.code.repository.OrderRepository;
import com.code.repositoryTestImpl.OrderRepositoryTestImpl;
import com.code.type.Courier;
import com.code.type.Order;
import com.code.type.OrderState;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class OrderService {

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CourierConverter courierConverter;

    public Collection<Order> getAllOrders() {
        return orderRepository.getAll();
    }

    public ArrayList<Order> getFreeOrders() {
        return orderRepository.getAll(OrderState.FREE);
    }

    public ArrayList<Order> getDeliveringOrders() {
        return orderRepository.getAll(OrderState.DELIVERING);
    }

    public ArrayList<Order> getDeliveringOrFreeOrders() {
        return orderRepository.getAll(OrderState.DELIVERING, OrderState.FREE);
    }

    public ArrayList<Order> getOrdersByCourier(Courier courier) {
        return orderRepository.getAllByCourier(courier);
    }

    public ArrayList<Order> getDeliveringOrCompletedOrdersByCourier(Courier courier) {
        return orderRepository.getAll(OrderState.DELIVERING, OrderState.COMPLETED);
    }

    public Order reserveOrder(Long id, Courier courier) throws Exception{
        Order order = orderRepository.getById(id);
        if (order == null)
            throw new IllegalArgumentException("Order cannot be null");
        order.reserveOrder(courier);
        return orderRepository.save(order);
    }

    public Order declineOrder(Long id) throws Exception{
        Order order = orderRepository.getById(id);
        if (order == null)
            throw new IllegalArgumentException("Order cannot be null");
        order.declineOrder();
        String contextUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        AccountEntity currentAccount = accountRepository.findByUsernameAndEnabled(contextUsername);
        CourierEntity courierEntity = currentAccount.getCourierEntity();
        Courier courier = courierConverter.modelToType(courierEntity);
        order.setCourier(courier);
        return orderRepository.save(order);
    }

    public Order completeOrder(Long id) throws Exception{
        Order order = orderRepository.getById(id);
        if (order == null)
            throw new IllegalArgumentException("Order cannot be null");
        order.takeOrder();
        String contextUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        AccountEntity currentAccount = accountRepository.findByUsernameAndEnabled(contextUsername);
        CourierEntity courierEntity = currentAccount.getCourierEntity();
        Courier courier = courierConverter.modelToType(courierEntity);
        order.setCourier(courier);
        return orderRepository.save(order);
    }

    public Order freeOrder(Long id) throws Exception{
        Order order = orderRepository.getById(id);
        if (order == null)
            throw new IllegalArgumentException("Order cannot be null");
        order.freeOrder();
        return orderRepository.save(order);
    }
}
