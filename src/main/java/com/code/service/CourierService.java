package com.code.service;

import com.code.repository.Repository;
import com.code.repositoryTestImpl.CourierRepositoryTestImpl;
import com.code.type.Courier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CourierService {

    public CourierService(Repository<Courier> courierRepository){
        this.courierRepository = courierRepository;
    }

    @Autowired
    private Repository<Courier> courierRepository;

    public ArrayList<Courier> getAll() {
        return (ArrayList<Courier>) courierRepository.getAll();
    }
}
