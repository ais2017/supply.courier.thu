package com.code.service;

import com.code.repository.OrderRepository;
import com.code.repositoryTestImpl.OrderRepositoryTestImpl;
import com.code.type.Order;
import com.code.type.OrderState;
import com.code.type.Route;

import java.util.ArrayList;
import java.util.Collection;

public class RouteService {

    public RouteService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    private OrderRepository orderRepository;

    public Route buildRoute(com.code.type.Courier courier) {
        Collection<Order> orders = orderRepository.getAllByCourier(courier, OrderState.DELIVERING, OrderState.DELIVERING);
        if (orders.size() == 0)
            throw new IllegalStateException("No delivering orders for courier found");
        Route route = new Route(new ArrayList<>(), 0);
        orders.forEach(m -> {
            route.addPoint(m.getDeliveryPoint().getMapPoint());
            route.addPoint(m.getTargetPoint());
        });
        return route;

    }
}
