package com.code.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.internal.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Account")
public class AccountEntity extends TransactionalEntity {

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private Boolean adminFlag;

    @Nullable
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "accountEntity", cascade = CascadeType.ALL)
    private CourierEntity courierEntity;

    public AccountEntity() {

    }

    public AccountEntity(String userName, String password) {
        super();
        this.setUsername(userName);
        this.setPassword(password);
    }

    public AccountEntity(Long id, String userName, String password) {
        super();
        this.setId(id);
        this.setUsername(userName);
        this.setPassword(password);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Boolean getAdminFlag() {
        return adminFlag;
    }

    public void setAdminFlag(Boolean adminFlag) {
        this.adminFlag = adminFlag;
    }

    public CourierEntity getCourierEntity() {
        return courierEntity;
    }

    public void setCourierEntity(CourierEntity courierEntity) {
        this.courierEntity = courierEntity;
    }
}