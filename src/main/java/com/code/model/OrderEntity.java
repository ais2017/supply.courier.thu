package com.code.model;


import com.code.type.OrderState;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "OrderEntity")
public class OrderEntity extends TransactionalEntity{

    private String clientName;

    private String clientPhone;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "courierId")
    private CourierEntity courier;

    @Column(name = "orderState", columnDefinition = "OrderState")
    @Type(type = "com.code.enumType.EnumTypeOrderState")
    private OrderState orderState;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "deliveryPointId")
    private DeliveryPointEntity deliveryPoint;

    private Date lastDeliveryDate;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "targetPointId")
    private MapPointEntity targetPoint;

    private int deliveryCounter;

    public OrderEntity() {
    }

    public OrderEntity(String clientName, String clientPhone, CourierEntity courier,
                       OrderState orderState, DeliveryPointEntity deliveryPointEntity,
                       Date lastDeliveryDate, MapPointEntity targetPoint, int deliveryCounter) {
        this.clientName = clientName;
        this.clientPhone = clientPhone;
        this.courier = courier;
        this.orderState = orderState;
        this.deliveryPoint = deliveryPointEntity;
        this.lastDeliveryDate = lastDeliveryDate;
        this.targetPoint = targetPoint;
        this.deliveryCounter = deliveryCounter;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    public CourierEntity getCourier() {
        return courier;
    }

    public void setCourier(CourierEntity courier) {
        this.courier = courier;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public DeliveryPointEntity getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setDeliveryPoint(DeliveryPointEntity deliveryPointEntity) {
        this.deliveryPoint = deliveryPointEntity;
    }

    public Date getLastDeliveryDate() {
        return lastDeliveryDate;
    }

    public void setLastDeliveryDate(Date lastDeliveryDate) {
        this.lastDeliveryDate = lastDeliveryDate;
    }

    public MapPointEntity getTargetPoint() {
        return targetPoint;
    }

    public void setTargetPoint(MapPointEntity targetPoint) {
        this.targetPoint = targetPoint;
    }

    public int getDeliveryCounter() {
        return deliveryCounter;
    }

    public void setDeliveryCounter(int deliveryCounter) {
        this.deliveryCounter = deliveryCounter;
    }
}
