package com.code.model;


import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MapPoint")
public class MapPointEntity extends TransactionalEntity {

    private Float lattitude;

    private Float longtitude;

    private String locationName;

    public MapPointEntity() {
    }

    public MapPointEntity(Float lattitude, Float longtitude, String locationName) {
        this.lattitude = lattitude;
        this.longtitude = longtitude;
        this.locationName = locationName;
    }

    public Float getLattitude() {
        return lattitude;
    }

    public void setLattitude(Float lattitude) {
        this.lattitude = lattitude;
    }

    public Float getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(Float longtitude) {
        this.longtitude = longtitude;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
