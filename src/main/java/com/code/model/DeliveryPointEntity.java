package com.code.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "DeliveryPoint")
public class DeliveryPointEntity extends TransactionalEntity {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "mapPointId")
    private MapPointEntity mapPointEntity;

    private String name;

    public DeliveryPointEntity() {
    }

    public DeliveryPointEntity(MapPointEntity mapPointEntity, String name) {
        this.mapPointEntity = mapPointEntity;
        this.name = name;
    }

    public MapPointEntity getMapPointEntity() {
        return mapPointEntity;
    }

    public void setMapPointEntity(MapPointEntity mapPointEntity) {
        this.mapPointEntity = mapPointEntity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
