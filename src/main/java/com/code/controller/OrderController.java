package com.code.controller;

import com.code.converter.CourierConverter;
import com.code.model.AccountEntity;
import com.code.model.CourierEntity;
import com.code.repositoryImpl.AccountRepository;
import com.code.repositoryImpl.CourierRepositoryImpl;
import com.code.service.CourierService;
import com.code.service.OrderService;
import com.code.type.Courier;
import com.code.type.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.nio.file.AccessDeniedException;
import java.util.Collection;

@Controller
public class OrderController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CourierRepositoryImpl courierRepositoryImpl;

    @Autowired
    private CourierConverter courierConverter;

    @Autowired
    private CourierService courierService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/api/couriers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Collection<Courier> getCouriers(
    ) throws Exception {
        String contextUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        AccountEntity currentAccount = accountRepository.findByUsernameAndEnabled(contextUsername);
        if (currentAccount == null || !currentAccount.getAdminFlag()) {
            throw new AccessDeniedException("Недостаточно прав для выполнения операции");
        }

        return courierService.getAll();
    }


    @RequestMapping(value = "/api/getAllOrders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Collection<Order> getAllOrders() throws Exception {
        String contextUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        AccountEntity currentAccount = accountRepository.findByUsernameAndEnabled(contextUsername);
//        if (currentAccount == null || !currentAccount.getAdminFlag()) {
//            throw new AccessDeniedException("sdf");
//        }

        Collection<Order> orders = orderService.getAllOrders();
        return orders;
    }

    @RequestMapping(value = "/api/getAvailableOrders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Collection<Order> getAvailableOrders(
    ) throws Exception {
        Collection<Order> orders = orderService.getFreeOrders();
        return orders;
    }

    @RequestMapping(value = "/api/getOrdersByCourier",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Collection<Order> getCourierOrdersTest(
            @RequestParam(value = "filterCourierId", required = true) long courierId
    ) throws Exception {
        CourierEntity courierEntity = courierRepositoryImpl.findById(courierId);
        if (courierEntity == null)
            return null;
        Courier courier = courierConverter.modelToType(courierEntity);
        Collection<Order> orders = orderService.getOrdersByCourier(courier);
        return orders;
    }

    @RequestMapping(value = "/api/getOwnOrders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Collection<Order> getCourierOrdersTest(
    ) throws Exception {
        String contextUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        AccountEntity currentAccount = accountRepository.findByUsernameAndEnabled(contextUsername);
        CourierEntity courierEntity = currentAccount.getCourierEntity();
        if(courierEntity == null)
            throw new Exception("Данный пользоавтель не является курьером");
        Courier courier = courierConverter.modelToType(courierEntity);
        Collection<Order> orders = orderService.getOrdersByCourier(courier);
        return orders;
    }

    @RequestMapping(value = "/api/getDeliveringOrders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Collection<Order> getDeliveringOrders(
    ) throws Exception {
        Collection<Order> orders = orderService.getDeliveringOrders();
        return orders;
    }

    @RequestMapping(value = "/api/reserveOrder",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Order reservingOrderTest(
            @RequestParam(value = "orderId", required = true) long orderId
    ) throws Exception {

        String contextUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        AccountEntity currentAccount = accountRepository.findByUsernameAndEnabled(contextUsername);
        CourierEntity courierEntity = currentAccount.getCourierEntity();
        if(courierEntity == null)
            throw new Exception("Данный пользоавтель не является курьером");
        Courier courier = courierConverter.modelToType(courierEntity);
        Order order = orderService.reserveOrder(orderId, courier);
        return order;
    }

    @RequestMapping(value = "/api/completeOrder",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Order completingOrderTest(
            @RequestParam(value = "orderId", required = true) long orderId
    ) throws Exception {
        Order order = orderService.completeOrder(orderId);
        return order;
    }

    @RequestMapping(value = "/api/declineOrder",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Order decliningOrderTest(
            @RequestParam(value = "orderId", required = true) long orderId
    ) throws Exception {
        Order order = orderService.declineOrder(orderId);
        return order;
    }



}
