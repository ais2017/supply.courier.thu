package com.code.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class HelloController {


    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


    @RequestMapping("/page")
    public Model hello(Model model,
                       @RequestParam(value = "action", required = false) String action,
                       @RequestParam(value = "voteId", required = false) Integer voteId,
                       @RequestParam(value = "voteDateTime", required = false) String voteDateTime,
                       @RequestParam(value = "voteAddress", required = false) String voteAddress) {

        model.addAttribute("votes", new String());
        return model;
    }

    @RequestMapping("/admin/adminPage")
    public static Model hello2(Model model,
                        @RequestParam(value = "action", required = false) String action,
                        @RequestParam(value = "personName", required = false) String personName,
                        @RequestParam(value = "personMiddleName", required = false) String personMiddleName,
                        @RequestParam(value = "personSurName", required = false) String personSurName,
                        @RequestParam(value = "address", required = false) String address,
                        @RequestParam(value = "phone", required = false) String phone,
                        @RequestParam(value = "birthdate", required = false) String birthdate,


                        @RequestParam(value = "addressVote", required = false) String addressVote,
                        @RequestParam(value = "description", required = false) String description,
                        @RequestParam(value = "voteDate", required = false) String voteDate,

                        @RequestParam(value = "voteId", required = false) Integer voteId,
                        @RequestParam(value = "personId", required = false) Integer personId,
                        @RequestParam(value = "voted", required = false) Boolean voted,

                        @RequestParam(value = "personVoteIds", required = false) String personVoteIds
    ) {

        model.addAttribute("personVote", new String());
        model.addAttribute("votess", new String());
        model.addAttribute("votes", new String());
        model.addAttribute("people", new String());
        model.addAttribute("peoplee", new String());
        model.addAttribute("personVotes", new String());

        return model;
    }

    @RequestMapping("/votes")
    public Model findVotes(Model model) {
        Map<String, String> country = new LinkedHashMap<String, String>();
        country.put("US", "United Stated");
        country.put("CHINA", "China");
        country.put("SG", "Singapore");
        country.put("MY", "Malaysia");
        model.addAttribute("voteList", country);
        return model;
    }

    @RequestMapping("/login")
    public Model login(Model model, @RequestParam(value = "name", required = false) String name) {

        return model;
    }

}
