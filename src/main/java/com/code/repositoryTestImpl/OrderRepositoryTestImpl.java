package com.code.repositoryTestImpl;

import com.code.repository.OrderRepository;
import com.code.type.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


public class OrderRepositoryTestImpl implements OrderRepository {
    public static String CLIENT_NAME = "Timofey Tiylenev";

    public static String CLIENT_PHONE = "88005553535";

    public static OrderState ORDER_STATE = OrderState.FREE;

    public static DeliveryPoint DELIVERY_POINT = DeliveryPointRepositoryTestImpl.generateDeliveryPoint();

    public static MapPoint TARGET_POINT = MapPointRepositoryTestImpl.generateMapPointList().get(0);

    public static int DELIVERY_COUNTER = 0;

    private static AtomicLong currentOrderId = new AtomicLong(0);

    public static Long incrementAndGetId() {
        return currentOrderId.incrementAndGet();
    }

    public static Map<Long, Order> orders = new HashMap<>();

    public Order save(Order order) {
        orders.put(order.getId(), order);
        return order;
    }

    public static Order generateOrder() {
        Long newId = incrementAndGetId();
        Order newVal = new Order(newId, CLIENT_NAME, CLIENT_PHONE, ORDER_STATE, DELIVERY_POINT, TARGET_POINT, DELIVERY_COUNTER);
        orders.put(newId,newVal);
        return newVal;
    }

    public static Collection<Order> generateOrderCollectionForTesting(Courier courier) {
        ArrayList<Order> orderArrayList = new ArrayList<>();
        Order freeOrder = generateOrder();
        orderArrayList.add(freeOrder);

        Order freeOrder2 = generateOrder();
        orderArrayList.add(freeOrder2);

        Order reservedOrder = generateOrder();
        reservedOrder.reserveOrder(courier);
        orderArrayList.add(reservedOrder);

        Order takenOrder = generateOrder();
        takenOrder.reserveOrder(courier);
        takenOrder.takeOrder();
        orderArrayList.add(takenOrder);

        return orderArrayList;
    }

    public static void clearAllForTesting() {
        orders.clear();
        currentOrderId.set(0);
    }

    public Order getById(Long id) {
        return orders.get(id);
    }

    public Collection<Order> getAll() {
        return orders.values();
    }

    public ArrayList<Order> getAll(OrderState orderState) {
        ArrayList ordersByOrderState = new ArrayList<>();
        orders.values().forEach(v -> {
            if (v.getOrderState().equals(orderState))
                ordersByOrderState.add(v);
        });
        return ordersByOrderState;
    }

    public ArrayList getAll(OrderState orderState1, OrderState orderState2) {
        ArrayList ordersByOrderState = new ArrayList<>();
        orders.values().forEach(v -> {
            if (v.getOrderState().equals(orderState1) || v.getOrderState().equals(orderState2))
                ordersByOrderState.add(v);
        });
        return ordersByOrderState;
    }

    public ArrayList getAllByCourier(Courier courier) {
        ArrayList ordersByCourier = new ArrayList<>();
        orders.values().forEach(v -> {
            if (v.getCourier() != null && v.getCourier().equals(courier))
                ordersByCourier.add(v);
        });
        return ordersByCourier;
    }

    public ArrayList<Order> getAllByCourier(Courier courier, OrderState orderState1, OrderState orderState2) {
        ArrayList<Order> ordersByCourier = new ArrayList<>();
        orders.values().forEach(v -> {
            if (v.getCourier() != null && v.getCourier().equals(courier) &&
                    (v.getOrderState().equals(orderState1) || v.getOrderState().equals(orderState2)))
                ordersByCourier.add(v);
        });
        return ordersByCourier;
    }


}
