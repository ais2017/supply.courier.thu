package com.code.repositoryTestImpl;

import com.code.type.MapPoint;

import java.util.Arrays;
import java.util.List;

public class MapPointRepositoryTestImpl {
    public static final String FIRST_MAP_POINT_NAME = "RostovOnDon";
    public static final String SECOND_MAP_POINT_NAME = "Krasnodar";


    public static List<MapPoint> generateMapPointList() {
        MapPoint mapPointRostov = new MapPoint(47.25F, 39.75F, FIRST_MAP_POINT_NAME);
        MapPoint mapPointKrasnodar = new MapPoint(45F, 39F, SECOND_MAP_POINT_NAME);
        return Arrays.asList(mapPointRostov, mapPointKrasnodar);
    }
}
