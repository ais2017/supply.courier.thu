package com.code.repositoryTestImpl;

import com.code.type.MapPoint;
import com.code.type.Route;

import java.util.ArrayList;

public class RouteRepositoryTestImpl {

    public static Route generateRoute() {
        MapPoint mapPointRostov = new MapPoint(47.25F, 39.75F, "RostovOnDon");
        MapPoint mapPointKrasnodar = new MapPoint(45F, 39F, "Krasnodar");
        ArrayList<MapPoint> mapPoints = new ArrayList<>(3);
        mapPoints.add(mapPointRostov);
        mapPoints.add(mapPointKrasnodar);
        return new Route(mapPoints, 0);
    }
}
