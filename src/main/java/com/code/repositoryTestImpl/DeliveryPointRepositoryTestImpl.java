package com.code.repositoryTestImpl;

import com.code.type.DeliveryPoint;
import com.code.type.MapPoint;

import java.util.Arrays;

public class DeliveryPointRepositoryTestImpl {
    public final static String MAP_POINT_NAME = "Theatre Square point";

    public static DeliveryPoint generateDeliveryPoint() {
        MapPoint mapPoint = MapPointRepositoryTestImpl.generateMapPointList().get(0);
        DeliveryPoint deliveryPoint = new DeliveryPoint(mapPoint, MAP_POINT_NAME , Arrays.asList());
        return deliveryPoint;
    }

}
