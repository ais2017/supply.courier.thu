package com.code.repositoryTestImpl;

import com.code.repository.Repository;
import com.code.type.Courier;
import com.code.type.Order;
import com.code.type.Route;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class CourierRepositoryTestImpl implements Repository<Courier> {

    private static AtomicLong courierId = new AtomicLong(0);

    public static Long incrementAndGetId() {
        return courierId.incrementAndGet();
    }

    public static Map<Long, Courier> couriers = new HashMap<>();

    public static String CLIENT_NAME = "Timofey Tiylenev";

    public static String CLIENT_PHONE = "88005553535";

    public ArrayList<Courier> getAll() {
        ArrayList <Courier> courierArrayList = new ArrayList<>();
        for(Courier courier:couriers.values()){
            courierArrayList.add(courier);
        }
        return courierArrayList;
    }

    public static Courier generateCourier() {

        Route route = RouteRepositoryTestImpl.generateRoute();
        long newKey = incrementAndGetId();
        Courier newVal = new Courier(newKey, CLIENT_NAME, CLIENT_PHONE, new ArrayList(), route);
        couriers.put(newKey,newVal);
        return newVal;
    }
}
