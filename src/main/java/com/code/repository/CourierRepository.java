package com.code.repository;

import com.code.type.Courier;

public interface CourierRepository extends Repository<Courier> {
}
