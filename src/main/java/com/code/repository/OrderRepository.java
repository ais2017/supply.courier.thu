package com.code.repository;

import com.code.type.Courier;
import com.code.type.Order;
import com.code.type.OrderState;

import java.util.ArrayList;

public interface OrderRepository extends Repository<Order> {
    public Order save(Order order) throws Exception;

    public ArrayList<Order> getAll(OrderState orderState);

    public ArrayList<Order> getAll(OrderState orderState1, OrderState orderState2);

    public ArrayList<Order> getAllByCourier(Courier courier);

    public ArrayList<Order> getAllByCourier(Courier courier, OrderState orderState1, OrderState orderState2);

    public Order getById(Long id);
}
