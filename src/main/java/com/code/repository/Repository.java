package com.code.repository;

import java.util.Collection;

@org.springframework.stereotype.Repository
public interface Repository<T> {

    public Collection<T> getAll ();

}
