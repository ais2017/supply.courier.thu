package com.code.converter;

import com.code.model.CourierEntity;
import com.code.model.OrderEntity;
import com.code.repositoryImpl.CourierRepositoryImpl;
import com.code.repositoryImpl.OrderReposioryImpl;
import com.code.type.DeliveryPoint;
import com.code.type.MapPoint;
import com.code.type.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Component
public class OrderConverter {
    @Autowired
    private OrderReposioryImpl orderReposiory;

    @Autowired
    private CourierRepositoryImpl courierRepositoryImpl;

    public Order modelToType(OrderEntity orderEntity) {

        MapPoint targetPoint = new MapPoint(orderEntity.getTargetPoint().getLattitude(),
                orderEntity.getTargetPoint().getLongtitude(),
                orderEntity.getTargetPoint().getLocationName());
        MapPoint mapPoint = new MapPoint(orderEntity.getDeliveryPoint().getMapPointEntity().getLattitude(),
                orderEntity.getDeliveryPoint().getMapPointEntity().getLongtitude(),
                orderEntity.getDeliveryPoint().getMapPointEntity().getLocationName());
        DeliveryPoint deliveryPoint = new DeliveryPoint(mapPoint, orderEntity.getDeliveryPoint().getName(), null);
        Order order = new Order(orderEntity.getId(), orderEntity.getClientName(), orderEntity.getClientPhone(),
                orderEntity.getOrderState(),
                deliveryPoint,
                targetPoint, orderEntity.getDeliveryCounter());

        return order;
    }

    public ArrayList<Order> collectionModelToType(Collection<OrderEntity> orderEntities) {

        ArrayList<Order> orders = new ArrayList<Order>(orderEntities.size());
        for (OrderEntity orderEntity : orderEntities) {
            orders.add(modelToType(orderEntity));
        }

        return orders;
    }

    public OrderEntity typeToModel(Order order) throws Exception {

        OrderEntity orderEntity = orderReposiory.findOne(order.getId());
        String contextUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        CourierEntity courierEntity = null;
        if (order.getCourier() != null)
            courierEntity = courierRepositoryImpl.findById(order.getCourier().getId());
        Date date = null;
        if (order.getLastDeliveryDate() != null)
            date = new Date(order.getLastDeliveryDate().getEra());
        OrderEntity orderEntityToReturn = new OrderEntity(order.getClientName(), order.getClientPhone(),
                courierEntity, order.getOrderState(),
                orderEntity.getDeliveryPoint(), date,
                orderEntity.getTargetPoint(), order.getDeliveryCounter());
        orderEntityToReturn.setId(order.getId());

        return orderEntityToReturn;
    }

}
