package com.code.converter;

import com.code.model.CourierEntity;
import com.code.model.OrderEntity;
import com.code.repositoryImpl.OrderReposioryImpl;
import com.code.type.MapPoint;
import com.code.type.Courier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class CourierConverter {

    @Autowired
    private OrderReposioryImpl orderReposiory;

    public Courier modelToType(CourierEntity courierEntity) {
        List<OrderEntity> orderList = orderReposiory.findAllByCourierId(courierEntity.getId());
        Courier courier = new Courier(courierEntity.getId(), courierEntity.getName(), courierEntity.getPhone(),
        orderList,null);

        return courier;
    }

    public ArrayList<Courier> collectionModelToType(Collection<CourierEntity> CourierEntities) {

        ArrayList<Courier> Couriers = new ArrayList<Courier>(CourierEntities.size());
        for (CourierEntity CourierEntity : CourierEntities) {
            Couriers.add(modelToType(CourierEntity));
        }

        return Couriers;
    }
}
