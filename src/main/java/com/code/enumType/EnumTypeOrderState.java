package com.code.enumType;

import com.code.model.OrderEntity;
import com.code.type.GenericEnumType;
import com.code.type.OrderState;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;


/**
 * Created by orlov on 26.04.17.
 */
public class EnumTypeOrderState extends GenericEnumType<String, OrderState> {

    public EnumTypeOrderState() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        super(OrderState.class, OrderState.values(), "getValue", Types.OTHER);
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names,
                              SessionImplementor session, Object owner)
            throws HibernateException, SQLException {
        return nullSafeGet(rs, names, owner);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index,
                            SessionImplementor session) throws HibernateException, SQLException {
        nullSafeSet(st, value, index);
    }

    public static class StringToEnum {
        public static OrderState getDocumentKindAsEnum(String documentKind) {
            if (documentKind.equals("FREE"))
                return OrderState.FREE;
            if (documentKind.equals("DELIVERING"))
                return OrderState.DELIVERING;
            if (documentKind.equals("COMPLETED"))
                return OrderState.COMPLETED;
            if (documentKind.equals("DECLINED"))
                return OrderState.DECLINED;
            throw new IllegalArgumentException("Invalid document type!");
        }
    }
}

