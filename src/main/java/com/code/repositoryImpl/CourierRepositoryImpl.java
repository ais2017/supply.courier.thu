package com.code.repositoryImpl;

import com.code.model.AccountEntity;
import com.code.model.CourierEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CourierRepositoryImpl extends JpaRepository<CourierEntity, Long> {

    @Query("SELECT a FROM CourierEntity a WHERE a.id = :id")
    CourierEntity findById(@Param(value = "id") Long id);
}
