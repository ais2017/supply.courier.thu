package com.code.repositoryImpl;

import com.code.model.OrderEntity;
import com.code.repository.OrderRepository;
import com.code.type.OrderState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderReposioryImpl extends JpaRepository<OrderEntity, Long> {

    @Query("SELECT a FROM OrderEntity a")
    List<OrderEntity> findAll(@Param("linkToBase") Long linkToBase);

    @Query("SELECT a FROM OrderEntity a WHERE a.courier.id =:courierId")
    List<OrderEntity> findAllByCourierId(@Param("courierId") Long courierId);

    @Query("SELECT a FROM OrderEntity a WHERE a.orderState =:orderState")
    List<OrderEntity> findAllByOrderState(@Param("orderState") OrderState orderState);

}
