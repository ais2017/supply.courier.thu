package com.code.type;


public enum OrderState {
    FREE("FREE"),
    DELIVERING("DELIVERING"),
    COMPLETED("COMPLETED"),
    DECLINED("DECLINED");

    protected String value;

    OrderState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }



}
