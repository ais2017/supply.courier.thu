package com.code.type;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Collection;

public class Courier {

    private Long id;
    private String name;
    private String phone;
    @JsonIgnore
    private Collection orders;
    @JsonIgnore
    private Route route;

    public Courier(Long id, String name, String phone, Collection orders, Route route) {
        if (id == null
                || name == null
                || phone == null
                || name.length() > 200
                || phone.length() > 200)
            throw new IllegalArgumentException("Invalid params for Courier class");

        this.id = id;
        this.name = name;
        this.phone = phone;
        this.orders = orders;
        this.route = route;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public Collection getOrders() {
        return orders;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    void addOrderForReserving(Order order) {
        if (order == null)
            throw new IllegalArgumentException("Invalid params for Courier class");
        orders.add(order);
    }

    void removeOrderWhileReserving(Order order) {
        orders.remove(order);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Courier)) return false;

        Courier courier = (Courier) o;

        return getId().equals(courier.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
