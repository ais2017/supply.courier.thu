/*
 * data.postgres script.
 * Create the database schema for the application.
 */

--DROP SEQUENCE IF EXISTS hibernate_sequence;
--CREATE SEQUENCE hibernate_sequence START WITH 1 INCREMENT BY 1;

DROP TABLE IF EXISTS Account CASCADE;
DROP TABLE IF EXISTS PersonVote CASCADE;
DROP TABLE IF EXISTS Vote CASCADE;
DROP TABLE IF EXISTS Person CASCADE;
DROP TABLE IF EXISTS Courier CASCADE;
DROP TABLE IF EXISTS OrderEntity CASCADE;
DROP TABLE IF EXISTS DeliveryPoint CASCADE;
DROP TABLE IF EXISTS MapPoint CASCADE;
DROP TYPE IF EXISTS OrderState CASCADE;

CREATE TYPE OrderState AS ENUM ('FREE','DELIVERING','COMPLETED','DECLINED');


CREATE TABLE Account (
  id BIGSERIAL,
  adminFlag BOOLEAN NOT NULL,
  username VARCHAR(100) NOT NULL,
  password VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
  );

  CREATE TABLE Courier (
  id BIGSERIAL,
  accountId BIGINT NOT NULL REFERENCES Account,
  name VARCHAR(100) NOT NULL,
  phone VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
  );

  CREATE TABLE MapPoint (
  id BIGSERIAL,
  lattitude FLOAT4 NOT NULL,
  longtitude FLOAT4 NOT NULL,
  locationName VARCHAR (100),
  PRIMARY KEY (id)
  );

  CREATE TABLE DeliveryPoint (
  id BIGSERIAL,
  mapPointId BIGINT NOT NULL REFERENCES MapPoint,
  name VARCHAR (100),
  PRIMARY KEY (id)
  );

  CREATE TABLE OrderEntity (
  id BIGSERIAL,
  clientName VARCHAR(100) NOT NULL,
  clientPhone VARCHAR(100) NOT NULL,
  courierId BIGINT REFERENCES Courier,
  orderState OrderState NOT NULL,
  deliveryPointId BIGINT REFERENCES DeliveryPoint,
  lastDeliveryDate TIMESTAMP    DEFAULT NULL,
  targetPointId BIGINT REFERENCES MapPoint,
  deliveryCounter INTEGER NOT NULL,
  PRIMARY KEY (id)
  );


