/*
 * data.postgres script.
 * Load the database with reference data and unit test data.
 */

-- password is 'password'
INSERT INTO Account (id,username, password, adminFlag)
 VALUES (1,'admin', '$2a$10$9/44Rne7kQqPXa0cY6NfG.3XzScMrCxFYjapoLq/wFmHz7EC9praK', true);
-- password is 'password'
INSERT INTO Account (id,username, password, adminFlag)
VALUES (2,'user', '$2a$10$9/44Rne7kQqPXa0cY6NfG.3XzScMrCxFYjapoLq/wFmHz7EC9praK', false);
-- password is 'password'
INSERT INTO Account (id,username, password, adminFlag)
VALUES (3,'otherUser', '$2a$10$9/44Rne7kQqPXa0cY6NfG.3XzScMrCxFYjapoLq/wFmHz7EC9praK', false);


INSERT INTO Courier (accountId,name, phone)
VALUES (2, 'Кондрат Ипатьев', '96123');

INSERT INTO Courier (accountId,name, phone)
VALUES (3, 'Михаил Булгаков', '96123');

INSERT INTO MapPoint (lattitude, longtitude, locationName)
VALUES (23.65, 23.65, 'Ростов-на-Дону, улица 20-я Линия');

INSERT INTO MapPoint (lattitude, longtitude, locationName)
VALUES (23.65, 23.65, 'Краснодар, ул. Красная');

INSERT INTO MapPoint (lattitude, longtitude, locationName)
VALUES (23.65, 23.65, 'Ростов-на-Дону (там, где тепло)');

INSERT INTO DeliveryPoint (mapPointId, name)
VALUES (1, 'Нахичеванский пункт выдачи в Ростове');

INSERT INTO DeliveryPoint (mapPointId, name)
VALUES (2, 'Rostov-on-Don');


INSERT INTO OrderEntity (clientName,clientPhone,courierId,orderState,
deliveryPointId,lastDeliveryDate,targetPointId,deliveryCounter)
VALUES ('user', '8023',1,'FREE',1,NULL,2,0);

INSERT INTO OrderEntity (clientName,clientPhone,courierId,orderState,
deliveryPointId,lastDeliveryDate,targetPointId,deliveryCounter)
VALUES ('otherUser', '8023',2,'FREE',1,NULL,2,0);
