/**
 * Created by sanyaorel on 03.10.16.
 */
//ALL ORDERS
function showAllOrders() {

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/getAllOrders",
        type: "GET",
        success: function (result, textStatus, jqXHR) {
            result.forEach(function (item, i, arr) {
                var dataTable = $('#dataTable');
                var tableHTML = "";
                var orderId = item["id"];
                tableHTML += '<tr>';
                tableHTML += '<td>' + item["id"] + '</td>';
                tableHTML += '<td>' + item["clientName"] + '</td>';
                tableHTML += '<td>' + item["clientPhone"] + '</td>';
                tableHTML += '<td>' + item["orderState"] + '</td>';
                tableHTML += '<td>' + item["deliveryPoint"]["name"] + '</td>';
                tableHTML += '<td>' + item["targetPoint"]["locationName"] + '</td>';

                var lastDeliveryDateString = "-";
                if (item["lastDeliveryDate"] != null)
                    lastDeliveryDateString = item["lastDeliveryDate"];
                tableHTML += '<td>' + lastDeliveryDateString + '</td>';
                tableHTML += '<td>' + item["deliveryCounter"] + '</td>';
                tableHTML += '</tr>';
                dataTable.append(tableHTML);
            });

        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}


//RESERVE ORDER
function getAvailableOrders() {

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/getAvailableOrders",
        type: "GET",
        success: function (result, textStatus, jqXHR) {
            result.forEach(function (item, i, arr) {
                var dataTable = $('#dataTable');
                var tableHTML = "";
                var orderId = item["id"];
                tableHTML += '<tr>';
                tableHTML += '<td>' + item["id"] + '</td>';
                tableHTML += '<td>' + item["clientName"] + '</td>';
                tableHTML += '<td>' + item["clientPhone"] + '</td>';
                tableHTML += '<td>' + item["orderState"] + '</td>';
                tableHTML += '<td>' + item["deliveryPoint"]["name"] + '</td>';
                tableHTML += '<td>' + item["targetPoint"]["locationName"] + '</td>';

                var lastDeliveryDateString = "-";
                if (item["lastDeliveryDate"] != null)
                    lastDeliveryDateString = item["lastDeliveryDate"];
                tableHTML += '<td>' + lastDeliveryDateString + '</td>';
                tableHTML += '<td>' + item["deliveryCounter"] + '</td>';
                tableHTML += '<td><button onclick="reserveOrder('+ orderId+')">Зарезервировать</button></td>';
                tableHTML += '</tr>';
                dataTable.append(tableHTML);
            });

        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}

function reserveOrder(id) {

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/reserveOrder?orderId=" + id,
        type: "POST",
        success: function (result, textStatus, jqXHR) {
            var dataTable = document.getElementById("dataTable");
            for(var i = dataTable.rows.length - 1; i > 1; i--)
            {
                dataTable.deleteRow(i);
            }
            alert("Действие успешно выполнено!");
            getAvailableOrders();
        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}

//TAKE ORDER
function getDeliveringOrders() {

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/getDeliveringOrders",
        type: "GET",
        success: function (result, textStatus, jqXHR) {
            result.forEach(function (item, i, arr) {
                var dataTable = $('#dataTable');
                var tableHTML = "";
                var orderId = item["id"];
                tableHTML += '<tr>';
                tableHTML += '<td>' + item["id"] + '</td>';
                tableHTML += '<td>' + item["clientName"] + '</td>';
                tableHTML += '<td>' + item["clientPhone"] + '</td>';
                tableHTML += '<td>' + item["orderState"] + '</td>';
                tableHTML += '<td>' + item["deliveryPoint"]["name"] + '</td>';
                tableHTML += '<td>' + item["targetPoint"]["locationName"] + '</td>';

                var lastDeliveryDateString = "-";
                if (item["lastDeliveryDate"] != null)
                    lastDeliveryDateString = item["lastDeliveryDate"];
                tableHTML += '<td>' + lastDeliveryDateString + '</td>';
                tableHTML += '<td>' + item["deliveryCounter"] + '</td>';
                tableHTML += '<td><button onclick="takeOrder('+ orderId+')">Зарезервировать</button>' +
                    '<button onclick="declineOrder('+ orderId+')">Отклонить</button></td>';
                tableHTML += '</tr>';
                dataTable.append(tableHTML);
            });

        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}

function takeOrder(id) {

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/completeOrder?orderId=" + id,
        type: "POST",
        success: function (result, textStatus, jqXHR) {
            var dataTable = document.getElementById("dataTable");
            for(var i = dataTable.rows.length - 1; i > 1; i--)
            {
                dataTable.deleteRow(i);
            }
            alert("Действие успешно выполнено!");
            getAvailableOrders();
        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}

//DECLINE ORDER
function declineOrder(id) {

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/declineOrder?orderId=" + id,
        type: "POST",
        success: function (result, textStatus, jqXHR) {
            var dataTable = document.getElementById("dataTable");
            for(var i = dataTable.rows.length - 1; i > 1; i--)
            {
                dataTable.deleteRow(i);
            }
            alert("Действие успешно выполнено!");
            getAvailableOrders();
        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}

//GET ORDERS BY COURIER
function getCouriers() {
    $( "#choose-station-wide" ).change(function() {
        getOrdersByCourier($( "#choose-station-wide" ).val());
    });

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/couriers",
        type: "GET",
        success: function (result, textStatus, jqXHR) {
            result.forEach(function (item, i, arr) {
                var optionTag = $('#choose-station-wide');
                var selectTagHTML = "";
                selectTagHTML += '<option value="'+item["id"]+'">';
                selectTagHTML += item["name"];
                selectTagHTML += '</option>';
                optionTag.append(selectTagHTML);
            });

        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}

function getOrdersByCourier(courierId) {

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/getOrdersByCourier?filterCourierId=" + courierId,
        type: "GET",
        success: function (result, textStatus, jqXHR) {
            result.forEach(function (item, i, arr) {
                var dataTable1 = document.getElementById("dataTable");
                for(var i = dataTable1.rows.length - 1; i > 1; i--)
                {
                    dataTable1.deleteRow(i);
                }
                var dataTable = $('#dataTable');
                var tableHTML = "";
                var orderId = item["id"];
                tableHTML += '<tr>';
                tableHTML += '<td>' + item["id"] + '</td>';
                tableHTML += '<td>' + item["clientName"] + '</td>';
                tableHTML += '<td>' + item["clientPhone"] + '</td>';
                tableHTML += '<td>' + item["orderState"] + '</td>';
                tableHTML += '<td>' + item["deliveryPoint"]["name"] + '</td>';
                tableHTML += '<td>' + item["targetPoint"]["locationName"] + '</td>';

                var lastDeliveryDateString = "-";
                if (item["lastDeliveryDate"] != null)
                    lastDeliveryDateString = item["lastDeliveryDate"];
                tableHTML += '<td>' + lastDeliveryDateString + '</td>';
                tableHTML += '<td>' + item["deliveryCounter"] + '</td>';
                tableHTML += '</tr>';
                dataTable.append(tableHTML);
            });

        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}

//GET OWN ORDERS

function getOwnOrders() {

    var requestJSON = $.ajax({
        dataType: "json",
        url: "../api/getOwnOrders",
        type: "GET",
        success: function (result, textStatus, jqXHR) {
            result.forEach(function (item, i, arr) {
                var dataTable = $('#dataTable');
                var tableHTML = "";
                var orderId = item["id"];
                tableHTML += '<tr>';
                tableHTML += '<td>' + item["id"] + '</td>';
                tableHTML += '<td>' + item["clientName"] + '</td>';
                tableHTML += '<td>' + item["clientPhone"] + '</td>';
                tableHTML += '<td>' + item["orderState"] + '</td>';
                tableHTML += '<td>' + item["deliveryPoint"]["name"] + '</td>';
                tableHTML += '<td>' + item["targetPoint"]["locationName"] + '</td>';

                var lastDeliveryDateString = "-";
                if (item["lastDeliveryDate"] != null)
                    lastDeliveryDateString = item["lastDeliveryDate"];
                tableHTML += '<td>' + lastDeliveryDateString + '</td>';
                tableHTML += '<td>' + item["deliveryCounter"] + '</td>';
                tableHTML += '</tr>';
                dataTable.append(tableHTML);
            });

        },
        statusCode: {},
        timeout: 20000
    })
        .fail(function (xhr, status) {
            alert(xhr.responseJSON["message"]);
        })
        .always(function (xhr, status) {
        });
}
