<div class="right-column">
    <table class="right-column-table black">
        <tr>
            <th colspan="3">Системные данные на момент последнего опроса</th>
        </tr>
        <tr>
            <td>Имя станции</td>
            <td>MS4-GMS</td>
        </tr>
        <tr>
            <td>Системный идентификатор</td>
            <td>1236</td>
        </tr>
        <tr>
            <td>Дата-время последнего опроса</td>
            <td>2015-02-30 23:32</td>
        </tr>
        <tr>
            <td>Системное дата-время станции в момент опроса</td>
            <td>2015-02-30 23:32</td>
        </tr>
        <tr>
            <td>Версия прошивки</td>
            <td>11.1.28 111</td>
        </tr>
    </table>
</div>